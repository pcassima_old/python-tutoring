# Python tricks

1. Calling a function on all elements in a list and store the result in a new list:

    ```python
    def my_func(elem):
        return elem_mod

    elems = [elem1, elem2, elem3, elem4, elem5]
    results = [my_func(elem) for elem in elems]
    ```

1. Checking if all elements in list match expression, using `all()`

    ```python
    elems = [elem1, elem2, elem3, elem4, elem5]
    if all([elem >= 0 for elem in elems]):
        print(True)
    ```
1. Checking if any element in list matches expression, using `any()`

	```python
    elems = [elem1, elem2, elem3, elem4, elem5]
    if any([elem >= 0 for elem in elems]):
        print(True)
   ```

# Debugging python in vscode

Debugging a program is an essential part of programming and is an essential part when the program isn’t working as expected. However programming itself can be quite a daunting task.

Luckily vscode makes this task a lot easier and hassle free.

## breakpoints

Breakpoints are the first and most important step in debugging. These tell the computer where to halt the program and start executing line by line.

Raised exceptions will act the same as breakpoints, in that they will halt the execution of the program.

When a breakpoint is hit, the program will halt. From that point the executing can be continued like normal (continue), step by step (step-over) or you can go into more detail when a function is called (step-into).

Breakpoints can be added to a specific line by clicking on the left of the line numbers (a red dot should appear).

## buttons

<img src="assets/Screenshot (91).png" style="zoom:150%;" />

The buttons from left to right are:

1. continue
1. step over
1. step into
1. step out
1. restart
1. stop

## steps

### start debugging

### step-over

### step-into

### step-out

### continue

