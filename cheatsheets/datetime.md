# Date & time in Python

Keeping track of dates and time can be tricky in Python, especially since there are two main packages that have very similar functionality. Both of these packages also come standard with Python. These packages are `datetime`, `time` and `calendar`.

## datetime module

### date class

### time class

### datetime class

### timedelta class

## time module

The time module, as the name would suggest, does everything that has to do with keeping the time. However it also offers some other functions related to time as well.

### time.sleep()



> Suspend execution of the calling thread for the given number of seconds. The argument may be a floating point number to indicate a more precise sleep time. The actual suspension time may be less than that requested because any caught signal will terminate the sleep() following execution of that signal’s catching routine. Also, the suspension time may be longer than requested by an arbitrary amount because of the scheduling of other activity in the system.
>
> Changed in version 3.5: The function now sleeps at least secs even if the sleep is interrupted by a signal, except if the signal handler raises an exception (see [PEP 475](https://www.python.org/dev/peps/pep-0475/) for the rationale).