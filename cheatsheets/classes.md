# Python classes

{:toc}

## Basic structure

The following snippet describes the basic structure of a class in Python:

```python
class ClassName(parent):
	def __init__(self):
		pass
```

In most cases the parent is the built-in `object` class, this however can be omitted.

```python
class ClassName(object):
    def __init__(self):
        pass
```

Each class needs a *initializer*, this is defined by the `__init__` method, documented later. 

## Different class methods

### Object method

These are methods that operate on the instance of a class or the object. As a result the first parameter for these methods is always `self`. When writing the method, this parameter has to be defined. However when calling the method, the `self` parameter has to be ignored; the Python interpreter will automatically assign the object you are working with, to this parameter when methods are called.

```python
class ClassName(parent):
	def methodName(self, params):
		pass
```

If we want to call this method, that would be done like in the following snippet; as mentioned the `self` parameter shouldn’t be filled.

```python
class ClassName(parent):
    def methodName(self, params):
        pass
    
if __name__ == '__main__':
    ClassName.methodName(params)
```

### Abstract method

An abstract method is mostly used in *parent* classes. This is done to indicate that these functions **have** to be implemented in the child classes. This can be useful when writing a parent class, and knowing that all child-classes will need a certain method, but the implementation will be different for each child-class.

However these can also be used to indicate plans for future development. In this case, the class is not sub-classed by any other class. 

```python
class ClassName(parent):
    def methodName(self, params):
        raise NotImplementedError
```

### Class method

These methods do not operate on the object or instance of a class, they operate on the class as a whole. Meaning that if this function changes class variables, they will change for all instances of the class. The same will happen when modifying a class variable directly from a object. 

However this is the preferred way to this, as it indicates to the user that they are operating on the class instead of the object. Avoiding unexpected problems where all instances have their values changed.

```python
class ClassName(parent):
	@classmethod
	def methodName(cls, params):
		pass
```

### Static method

A static method is in essence a “normal” function. In that it does not operate on the class nor the object. It takes input and gives an output.

Static methods can be used to check variables, or format variables for outputting etc. In short they are used for storing separate functions, that are still related to the class and not used outside of the class.

```python
class ClassName(parent):
    @staticmethod
    def methodName(params):
        pass
```

## Returning from a class

### Returning a new object of the same class

The following snippet will return a **new** object of the same class. The same could be achieved by just calling the *class-name* directly. However by using the following structure, no issues are created when renaming the class or when the class is sub-classed.

```python
class ClassName(parent):
    def __init__(self, init_params):
        pass
    
    def MethodName(self, params):
        return self.__class__(init_params)
```

### Returning a reference to the object on which the method was called

The following snippet shows how to return a reference to the object that the method was called on in the first place.

```python
class ClassName(parent):
    def MethodName(self, params):
        return self
```

This is used methods are called on the output of another method, like in the following example:

```python
class ClassName(parent):
    def MethodAName(self, paramsA):
        return self
    def MethodBName(self, paramsB):
        pass
if __name__ == "__main__":
    instance = ClassName(params)
    instance.MethodAName(paramsA).MethodBName(paramsB)
```

## Magic methods

### Constructors and initialization

| Method                   | Description                                                  |
| ------------------------ | ------------------------------------------------------------ |
| `__new__(cls, params)`   | The **constructor** of a class, used for creating a new object of the class. Although used rarely it is useful when working with immutable types (tuples & strings). Any arguments that it receives are passed on to ```__init__```. |
| `__init__(self, params)` | This is the **initializer **for a class. It gets all the arguments that primary constructor was called with. This method is used in almost all Python class definitions. |
| `__del__(self)`          | This method forms the **destructor **of the class. This method is called when the object is cleaned by the garbage collection. Although it can be useful for classes that need extra actions upon deletion. However there is no guarantee that the destructor will be called when the object exists, when the interpreter exits. As such it should be used with caution. |

### Comparison methods

In the comparison methods ‘self’ is always on the left side of the operator, while ‘other’ is on the right side.

| Method                 | Description                                                  |
| ---------------------- | ------------------------------------------------------------ |
| `__cmp__(self, other)` | This is the most general comparison method. It implements behaviour for all the comparison operators (>, ==, <=, etc). this method should return a negative integer if *self < other*, zero if *self == other*  and a positive integer if *self > other*. This method can be useful to avoid repetition, however it is better to define each comparison method separately, especially if there are different conditions that determine if self > other and if self < other. |
| `__eq__(self, other)`  | Is called when the ‘==’ operator is used.                    |
| `__ne__(self, other)`  | Is called when the ‘!=’ operator is used.                    |
| `__lt__(self, other)`  | Is called when the ‘<’ operator is used.                     |
| `__gt__(self, other)`  | Is called when the ‘>’ operator is used.                     |
| `__le__(self, other)`  | Is called when the ‘<=’ operator is used.                    |
| `__ge__(self, other)`  | Is called when the ‘=>’ operator is used.                    |

### Unary operators and functions

| Method               | Description                                                  |
| -------------------- | ------------------------------------------------------------ |
| `__pos__(self)`      | Implements behaviour for unary positive (e.g. ```+object```). |
| `__neg__(self)`      | Implements behaviour for negation (e.g. ```-object```).      |
| `__abs__(self)`      | Is executed when the abs() is called on the object.          |
| `__invert__(self)`   | Implements bitwise negation, which is done with the ‘~’ operator |
| `__round__(self, n)` | Is executed when the built-in ```round()``` function is called on the object. ```n``` is the amount of decimal places to round to. |
| `__floor__(self)`    | Implements behaviour for ```math.floor()```. It will round down to the nearest integer. |
| `__ceil__(self)`     | Implements behaviour for ```math.ceil()```. It will round up to the nearest integer. |
| `__trunc__(self)`    | Implements behaviour for ```math.trunc()```. It will truncate to an integral. |

### Arithmetic operators

#### Math operators

| Method                      | Operator | Description                                                  |
| :-------------------------- | -------- | ------------------------------------------------------------ |
| `__add__(self, other)`      | +        | Implements addition.                                         |
| `__sub__(self, other)`      | -        | Implements subtraction.                                      |
| `__mul__(self, other)`      | *        | Implements multiplication.                                   |
| `__div__(self, other)`      | /        | Implements division.                                         |
| `__floordiv__(self, other)` | //       | Implements integer division.                                 |
| `__truediv__(self, other)`  |          | Implements true division. Will only work when ```from __future__ import division``` is in effect. |
| `__mod__(self, other)`      | %        | Implements modulo.                                           |
| `__divmod__(self, other)`   |          | Implements long division, using the ```divmod()``` built-in function. |
| `__pow__(self, other)`      | **       | Implements exponents.                                        |

#### Binary operators

| Method                    | Operator | Description                     |
| ------------------------- | -------- | ------------------------------- |
| `__and__(self, other)`    | &        | Implements bitwise **AND**.     |
| `__or__(self, other)`     | \|       | Implements bitwise **OR**.      |
| `__xor__(self, other)`    | ^        | Implements bitwise **XOR**.     |
| `__lshift__(self, other)` | <<       | Implements bitwise left shift.  |
| `__rshift__(self, other)` | >>       | Implements bitwise right shift. |

### Reflected arithmetic operators

Reflected arithmetic is a sort of ‘back-up’ solution.

If we look at this case as being the normal addition.

```python
some_object + other
```

The reflected addition becomes:

```python
other + some_object
```

The methods do the same as there normal equivalents, except with the operands switched places. The reflected operator is called when the object on the left side of the operator (`other` in this case) does not define (or returns *NotImplemented*) for its definition of the normal version of the operation.

For example `some_object.__radd__` will only be called if `other` does not define `__add__`.

#### Reflected math operators

| Method                       | Description                                                  |
| :--------------------------- | ------------------------------------------------------------ |
| `__radd__(self, other)`      | Implements reflected addition.                               |
| `__rsub__(self, other)`      | Implements reflected subtraction.                            |
| `__rmul__(self, other)`      | Implements reflected multiplication.                         |
| `__rdiv__(self, other)`      | Implements reflected division.                               |
| `__rfloordiv__(self, other)` | Implements reflected integer division.                       |
| `__rtruediv__(self, other)`  | Implements reflected true division. Will only work when `from __future__ import division` is in effect. |
| `__rmod__(self, other)`      | Implements reflected modulo.                                 |
| `__rdivmod__(self, other)`   | Implements reflected long division, using the `divmod()` built-in function, when `divmod(other, self)` is called. |
| `__rpow__(self, other)`      | Implements reflected exponents.                              |

#### Reflected binary operators

| Method                     | Operator | Description                               |
| -------------------------- | -------- | ----------------------------------------- |
| `__rand__(self, other)`    | &        | Implements reflected bitwise **AND**.     |
| `__ror__(self, other)`     | \|       | Implements reflected bitwise **OR**.      |
| `__rxor__(self, other)`    | ^        | Implements reflected bitwise **XOR**.     |
| `__rlshift__(self, other)` | <<       | Implements reflected bitwise left shift.  |
| `__rrshift__(self, other)` | >>       | Implements reflected bitwise right shift. |

### Augmented assignment

```python
# Normal addition
x = x + 5
# Augmented assignment
x += 5
```

#### Math operators with assignments

| Method                       | Operator | Description                                                  |
| :--------------------------- | -------- | ------------------------------------------------------------ |
| `__iadd__(self, other)`      | +=       | Implements addition with assignment.                         |
| `__isub__(self, other)`      | -=       | Implements subtraction with assignment.                      |
| `__imul__(self, other)`      | *=       | Implements multiplication with assignment.                   |
| `__idiv__(self, other)`      | /=       | Implements division with assignment.                         |
| `__ifloordiv__(self, other)` | //=      | Implements integer division with assignment.                 |
| `__itruediv__(self, other)`  |          | Implements true division with assignment. Will only work when `from __future__ import division` is in effect. |
| `__imod__(self, other)`      | %=       | Implements modulo with assignment.                           |
| `__ipow__(self, other)`      | **=      | Implements exponents with assignments.                       |

#### Binary operators with assignments

| Method                     | Operator | Description                     |
| -------------------------- | -------- | ------------------------------- |
| `__iand__(self, other)`    | &=       | Implements bitwise **AND**.     |
| `__ior__(self, other)`     | \|=      | Implements bitwise **OR**.      |
| `__ixor__(self, other)`    | ^=       | Implements bitwise **XOR**.     |
| `__ilshift__(self, other)` | <<=      | Implements bitwise left shift.  |
| `__irshift__(self, other)` | >>=      | Implements bitwise right shift. |

### Type conversion

| Method              | Function     | Description                                                  |
| ------------------- | ------------ | ------------------------------------------------------------ |
| `__int__(self)`     | int()        | Implements type conversion to int.                           |
| `__long__(self)`    | long()       | Implements type conversion to long. Became obsolete in Python 3.x, as the int type is now of unlimited length (and size). |
| `__float__(self)`   | float()      | Implements type conversion to float.                         |
| `__complex__(self)` | complex()    | Implements type conversion to complex.                       |
| `__oct__(self)`     | oct()        | Implements type conversion to octal.                         |
| `__hex__(self)`     | hex()        | Implements type conversion to hexadecimal.                   |
| `__trunc__(self)`   | math.trunc() | Called when `math.trunc(self)` is called. `__trunc__` should return the value of self truncated to an integral type (usually a long). |
| `__coerce__(self)`  |              | Method to implement mixed mode arithmetic (mixed type). `__coerce__` should return `None` if type conversion is impossible. Otherwise, it should return a pair (tuple) of `self` and `other`, manipulated to have the same type. |

### Representing classes



## References

rszalski on Github for most of the documentation on the magic methods [rszalski](https://rszalski.github.io/magicmethods/ "magic methods by rszalski").

