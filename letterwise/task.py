def read_database(textFile: str) -> dict:
    with open(textFile, 'r') as inFile:
        result = {}
        for line in inFile.read().strip().split('\n'):
            key, value = line.split(',')
            result[key] = value
        return result


def combinations(code: str) -> list:
    result = []
    while code:
        try:
            char = code[0]
            next_char = code[1]
            if next_char.isalpha():
                code = code[2:]
                to_append = char + next_char
                try:
                    while code[0].isalpha():
                        to_append += code[0]
                        code = code[1:]
                except IndexError:
                    result.append(to_append)
                    break
                result.append(to_append)
            else:
                result.append(char)
                code = code[1:]
        except IndexError:
            result.append(char)
            break
    return result


def letter(prefix: str, combination: str, dictionary: dict) -> str:
    lut = {
        2: ['A', 'B', 'C'],
        3: ['D', 'E', 'F'],
        4: ['G', 'H', 'I'],
        5: ['J', 'K', 'L'],
        6: ['M', 'N', 'O'],
        7: ['P', 'Q', 'R', 'S'],
        8: ['T', 'U', 'V'],
        9: ['W', 'X', 'Y', 'Z'],
    }
    try:
        if len(prefix) <= 3:
            possibilities = [char for char in dictionary[prefix]]
        else:
            prefix = prefix[-3:]
            possibilities = [char for char in dictionary[prefix]]
    except KeyError:
        # A non existing prefix is the same as no prefix.
        possibilities = [char for char in dictionary['']]
    key = int(combination[0])
    actual_pos = []
    for char in possibilities:
        if char in lut[key]:
            actual_pos.append(char)
    next_amount = combination.count('N')
    result = actual_pos[next_amount % len(actual_pos)]
    return result


def word(comb_str: str, dictionary: dict) -> str:
    comb_list = combinations(comb_str)
    result = ''
    for comb in comb_list:
        if len(result) > 3:
            prefix = result[-3:]
        else:
            prefix = result
        result += letter(prefix, comb, dictionary)
    return result


if __name__ == "__main__":
    database_en = read_database('letterwise/en.01.txt')
    word('9N2837N', database_en)
