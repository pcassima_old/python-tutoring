"""
Adjacent numbers

Copyright (C) 2019, Pieter-Jan Cassiman

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# --------------------------------- Imports -----------------------------------


# ----------------------------- Global variables ------------------------------

__author__ = "P. Cassiman"
__version__ = '1.0.0'

# --------------------------------- Classes -----------------------------------


# -------------------------------- Functions ----------------------------------

def neighbours(row, column, matrix):

    row_index = row
    column_index = column

    result = set()

    if row_index < 0 or row_index > len(matrix) - 1:
        raise AssertionError('invalid position')

    if column_index < 0 or column_index > len(matrix[0]) - 1:
        raise AssertionError('invalid position')

    # TODO: write as for-loop
    # 1. Check the top row
    top_index = row_index - 1
    if top_index < 0 or top_index > len(matrix) - 1:
        pass
    else:
        top_row = matrix[top_index]
        # left
        left_index = column_index - 1
        if left_index < 0 or left_index > len(matrix) - 1:
            pass
        else:
            left = top_row[left_index]
            result.add(left)
        # Middle
        middle_index = column_index
        middle = top_row[middle_index]
        result.add(middle)
        # Right
        right_index = column_index + 1
        if right_index < 0 or right_index > len(matrix) - 1:
            pass
        else:
            right = top_row[right_index]
            result.add(right)
    # 2. Check the current row
    left_index = column_index - 1
    current_row = matrix[row_index]
    if left_index < 0 or left_index > len(matrix) - 1:
        pass
    else:
        left = current_row[left_index]
        result.add(left)

    right_index = column_index + 1
    if right_index < 0 or right_index > len(matrix) - 1:
        pass
    else:
        right = current_row[right_index]
        result.add(right)

    # 3. Check the bottom row
    bottom_index = row_index + 1
    if bottom_index < 0 or bottom_index > len(matrix) - 1:
        pass
    else:
        bottom_row = matrix[bottom_index]
        # left
        left_index = column_index - 1
        if left_index < 0 or left_index > len(matrix) - 1:
            pass
        else:
            left = bottom_row[left_index]
            result.add(left)
        # Middle
        middle_index = column_index
        middle = bottom_row[middle_index]
        result.add(middle)
        # Right
        right_index = column_index + 1
        if right_index < 0 or right_index > len(matrix) - 1:
            pass
        else:
            right = bottom_row[right_index]
            result.add(right)

    return result


# ----------------------------------- Main ------------------------------------

if __name__ == "__main__":
    pass
