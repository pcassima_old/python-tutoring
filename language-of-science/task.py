word1 = input()
word2 = input()

longest = ''
shortest = ''

if len(word1) >= len(word2):
    longest = word1
    shortest = word2
else:
    longest = word2
    shortest = word1

front = ''
end = ''

for i in range(len(longest)):
    if longest[i] == shortest[i]:
        front += longest[i]
    else:
        break

for i in range(len(longest)):
    if longest[-1 - i] == shortest[-1 - i]:
        end += longest[-1 - i]
    else:
        break

end = end[::-1]

word1 = word1.replace(front, '', 1)
word2 = word2.replace(front, '', 1)

longest = longest.replace(front, '', 1)
longest = longest.replace(end, '', 1)

word1 = word1.replace(end, '', 1)
word2 = word2.replace(end, '', 1)

whitespace = len(word1) - len(word2)

result = ''

word1 = word1.center(len(word2), ' ')
word2 = word2.center(len(word1), ' ')

top_line = (' ' * len(front)) + '┏' + word1 + '┓' + '\n'
middle_line = front + '┫' + (' ' * len(longest)) + '┣' + end + '\n'
bottom_line = (' ' * len(front)) + '┗' + word2 + '┛'

result = top_line + middle_line + bottom_line
print(result)
