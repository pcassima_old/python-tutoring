"""
Copyright (C) 2019, Pieter-Jan Cassiman

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# --------------------------------- Imports -----------------------------------


# ----------------------------- Global variables ------------------------------

__author__ = "P. Cassiman"
__version__ = '1.0.0'


# --------------------------------- Classes -----------------------------------


# -------------------------------- Functions ----------------------------------


def chemifyWord(word: str) -> str:
    """
    This function removes all vowels from the end of a word and replaces it
    with "ium".

    The try-except structure for checking if the word is a number works, but
    is not really a "clean" way of doing this...

    Arguments:
        word {str} -- The word that needs to be modified.

    Returns:
        str -- The chemified word.
    """

    # A list with all vowels
    vowels = ['a', 'e', 'i', 'o', 'u', 'y']

    # If the word is empty, return the empty word and stop the function.
    if not word:
        return word

    # Check if the word is a number (not really a clean way to do it).
    try:
        # If the is a number, the conversion will work and the return statement
        # will be executed.
        int(word)
        return word
    except ValueError as e:
        # If the conversion fails, just continue with the word (it isn't a
        # number).
        pass

    # Isolate the last 3 characters of the word.
    end = word[-3:].lower()
    if end == 'ium':
        # If the last 3 characters are "ium", return the word.
        return word

    # Start with last character of the word
    last_char = word[-1].lower()
    # As long as the last character is in vowels keep looping
    while last_char in vowels:
        # Remove the last character from the word
        word = word[:-1]
        # If something is left in the word
        if word:
            # Save a new last character
            last_char = word[-1].lower()
        else:
            # If there is nothing left break out of the loop.
            break

    # Adding ium to word
    word += 'ium'
    # Return the word
    return word


def chemify(sentence: str) -> str:
    """
    Function to chemify a whole sentence. The sentence gets split up into
    separate words. Each word is then chemified and placed together to reform
    the sentence. All the punctuation is maintained.

    The function is not completely "perfect" yet. The removing of characters
    after the loops, can be avoided and programmed more efficiently.
    However I cannot think of how to do it now, nevertheless the function
    works.

    Arguments:
        sentence {str} -- The sentence to chemify

    Returns:
        str -- The chemified sentence
    """

    # Split the sentence into different words (split on space).
    words = sentence.split(" ")
    # A list with all the punctuation characters.
    punctuation = [",", ".", "!", "-", ":", ")"]
    # Start a variable for the last character (to save the punctuation).
    last_char = ""
    # Reset the sentence.
    sentence = ""

    # Go over each word in the list (sentence).
    for word in words:
        # Check to see if the word ends in punctuation.
        if word[-1].lower() in punctuation:
            # If it ends in punctuation, save it for later.
            last_char = word[-1]
            # If the last character is a dash.
            if last_char == "-":
                # Check if the one before that is also a dash.
                if word[-2] == "-":
                    # If it is also save it for later.
                    last_char += "-"
                    # Remove the last character (one extra time, because there
                    # are two punctuation characters).
                    word = word[:-1]
            # Remove the last character (punctuation) from the word.
            word = word[:-1]

        # If there is at least one dash in (the middle of) the word.
        if "-" in word:
            # Split the word again (on the dashes).
            dashed_words = word.split("-")
            # Reset the word.
            word = ""

            for dashed_word in dashed_words:
                # For each word that was split, chemify the word and add it to
                # the original word.
                word += chemifyWord(dashed_word)
                # Add a dash after the chemified word.
                word += "-"
            # After this remove the last character (the last dash).
            word = word[:-1]

        else:
            # If no dashes are in the word, just chemify the word.
            word = chemifyWord(word)

        # Add the last character (punctuation) back to the word.
        word += last_char
        # Reset the last char variable.
        last_char = ""
        # Add the word to the sentence.
        sentence += word
        # Add a space to the sentence, after the word
        sentence += " "

    # When the whole sentence is finished remove the last space.
    sentence = sentence[:-1]
    # Return the sentence.
    return sentence


# ----------------------------------- Main ------------------------------------


if __name__ == "__main__":
    print(chemify('University of -- california, Berkeley 10'))
