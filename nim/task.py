"""
Nim

Copyright (C) 2019, Pieter-Jan Cassiman

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# --------------------------------- Imports -----------------------------------

from __future__ import annotations
from typing import Union

# ----------------------------- Global variables ------------------------------

__author__ = "P. Cassiman"
__version__ = '1.0.0'

# --------------------------------- Classes -----------------------------------


class Nim(object):
    def __init__(self, sequence: Union[list, tuple]):
        assert sequence is not None, 'invalid heaps'
        assert isinstance(sequence, (list, tuple)), 'invalid heaps'
        assert len(sequence) >= 2, 'invalid heaps'
        try:
            if any([x < 0 or not isinstance(x, int) for x in sequence]):
                raise AssertionError('invalid heaps')
        except TypeError:
            raise AssertionError('invalid heaps')

        self.heaps = list(sequence)

    def __len__(self) -> int:
        return len(self.heaps)

    def __repr__(self) -> str:
        return self.__class__.__name__ + '(' + str(self.heaps) + ')'

    def __str__(self) -> str:
        result = ''
        for index, heap in enumerate(self.heaps):
            result += str(index + 1) + ': ' + '|' * heap + '\n'
        return result[:-1]

    def __add__(self, other: Nim) -> Nim:
        new_heaps = []
        if len(self) > len(other):
            for index, heap in enumerate(self.heaps):
                try:
                    new_heaps.append(heap + other.heaps[index])
                except IndexError:
                    new_heaps.append(heap)
        else:
            for index, heap in enumerate(other.heaps):
                try:
                    new_heaps.append(heap + self.heaps[index])
                except IndexError:
                    new_heaps.append(heap)
        return self.__class__(new_heaps)

    def remove(self, heap: int, number: int) -> self:
        heap -= 1

        # No negative indices.
        assert heap >= 0, 'invalid move'
        # No going out of range.
        assert heap < len(self), 'invalid move'

        # Cannot remove more matches, than there are.
        assert number <= self.heaps[heap], 'invalid move'
        # At least one match has to be removed.
        assert number > 0, 'invalid move'

        self.heaps[heap] = self.heaps[heap] - number
        return self

    def won(self) -> bool:
        return all(x == 0 for x in self.heaps)

# -------------------------------- Functions ----------------------------------


# ----------------------------------- Main ------------------------------------


if __name__ == "__main__":
    pass
