def reduce(keyword: str) -> str:
    reduced = ''
    for char in keyword:
        if char not in reduced:
            reduced += char
    return reduced


def _create_encode_lut(keyword):
    lut = {}
    for index, char in enumerate(reduce(keyword), start=1):
        lut[str(index % 10)] = char
    return lut


def _create_decode_lut(keyword):
    lut = {}
    for index, char in enumerate(reduce(keyword), start=1):
        lut[char] = str(index % 10)
    return lut


def encode(number: int, keyword: str) -> str:
    lut = _create_encode_lut(keyword)
    encoded = ''
    number = str(number)
    for char in number:
        encoded += lut[char]

    return encoded


def decode(encoded: str, keyword: str) -> int:
    lut = _create_decode_lut(keyword)
    decoded = ''

    for char in encoded:
        decoded += lut[char]

    decoded = int(decoded)
    return decoded


def next(encoded: str, keyword: str) -> str:
    decoded = decode(encoded, keyword)
    decoded += 1
    new_encoded = encode(decoded, keyword)
    return new_encoded


if __name__ == "__main__":
    pass
