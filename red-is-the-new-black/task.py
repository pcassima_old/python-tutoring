"""
Copyright (C) 2019, Pieter-Jan Cassiman

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# --------------------------------- Imports -----------------------------------

from __future__ import annotations
from typing import Union

# ----------------------------- Global variables ------------------------------

__author__ = "P. Cassiman"
__version__ = '1.0.0'

# --------------------------------- Classes -----------------------------------


class Stack(object):
    # Create a list with all valid value-strings for the cards
    values = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K']
    # Create a list with all strings for the suits
    suits = ['C', 'D', 'H', 'S']

    def __init__(self, description: Union[list, tuple]):
        # Create a list to store all the cards associated with the object.
        self.cards = []

        # Check if the description has the right datatype, if it does not have
        # the right datatype raise an error.
        if not isinstance(description, (list, tuple)):
            raise AssertionError("invalid stack")

        # If everything is right, we can go over each card in the
        # description.
        for card in description:
            # Check if the card is a string, if not, raise an error.
            if not isinstance(card, str):
                raise AssertionError("invalid stack")

            # Extract the suit from the card.
            suit = card[-1]
            # Extract the value from the card.
            value = card[:-1]

            # Check if both the value and the suit appear in the respective
            # lists. if not raise an error.
            # Here it is also checked to see if the card is not already in
            # the list, if it is raise an error.
            if (suit not in self.suits or
                value not in self.values or
                    card in self.cards):
                raise AssertionError("invalid stack")

            # If all checks pass, add the card to the cards list.
            self.cards.append(card)

    def __len__(self) -> int:
        """
        Method to return the amount of cards there are in the Stack. For this
        the length of the cards list is returned.

        Returns:
            int -- amount of cards in the stack.
        """

        return len(self.cards)

    def __str__(self) -> str:
        """
        Method to return a string representation of the stack of cards.
        For this, each card is added to the string, separated by a space.

        Returns:
            str -- All the cards in the stack represented as a string.
        """

        # Start with a blank string
        result = ''

        for card in self.cards:
            # Add each card to the string.
            result += card
            # Followed by a space.
            result += ' '
        # Finally remove the trailing space.
        result = result[:-1]
        # Return the result.
        return result

    def __repr__(self) -> str:
        """
        Returns the classname as well as the representation of the cards in the
        deck, as a string.

        Returns:
            str -- The verbose representation of the string.
        """

        # Create a string and format in the class name and the result of the
        # __str__ method.
        result = '{}({})'.format(self.__class__.__name__, str(self.cards))
        return result

    def __add__(self, other: Stack) -> Stack:
        """
        Method to add two stacks together. When executing this method the
        cards in the other stack have to be placed under the self stack.

        Arguments:
            other {Stack} -- The stack to add to this stack.

        Returns:
            Stack -- Returns a new Stack object that has the cards of both
                     stacks.
        """

        # Create a list, consisting of the sum of both Stacks' lists.
        total_cards = self.cards + other.cards

        # Return a new Stack object with these cards.
        return self.__class__(total_cards)

    def draw(self, amount: int) -> Stack:
        """
        Method to draw x-amount of cards from the stack. The cards are drawn
        from the top (and removed from the current stack).

        The drawn cards are returned as a new Stack object.

        Arguments:
            amount {int} -- The amount of cards to draw from the stack.

        Raises:
            AssertionError: When trying to draw more cards then there are in
                            the stack.

        Returns:
            Stack -- A new Stack object with the drawn cards in them.
        """

        # If you are trying to draw more cards than there are in the stack.
        if amount > self.__len__():
            # Raise an error
            raise AssertionError("invalid number of cards")

        # Draw the given amount of cards from the Stack.
        new_stack = self.cards[:amount]

        # Remove the drawn amount of cards from the stack.
        self.cards = self.cards[amount:]

        # Return a new Stack object with the drawn cards.
        return self.__class__(new_stack)

    def deal(self) -> tuple:
        """
        Method to deal cards according to the rules outlined in the task
        description.

        Take two cards, if they are the same colour put them in their
        respective Stack (red - or black stack). If they have a different
        colour the cards can be discarded (thrown away).

        Raises:
            AssertionError: When there is an odd number of cards in the deck
                            and not all cards can be divided in pairs.

        Returns:
            tuple -- A tuple containing the red and black stack objects,
                     respectively.
        """

        # Raise an error when there is an odd number of cards.
        if self.__len__() % 2:
            raise AssertionError("odd number of cards")

        # Initialise a red and black stack list.
        red_stack = []
        black_stack = []

        # As long as there are cards in the current stack, keep looping.
        while self.cards:

            # Take a pair of cards from the bottom of the stack.
            # Take the before-last card.
            card1 = self.cards[-2]
            # And the last card.
            card2 = self.cards[-1]

            # Check if both cards are red.
            if 'D' in card1 or 'H' in card1:
                # If the first one is a red card.
                if 'D' in card2 or 'H' in card2:
                    # The second one is also a red card, add them to the red
                    # stack.
                    red_stack.append(card1)
                    red_stack.append(card2)

            # Check if both cards are black.
            elif 'C' in card1 or 'S' in card1:
                # If the first one is a black card.
                if 'C' in card2 or 'S' in card2:
                    # The second one is also a red card, add them to the black
                    # stack.
                    black_stack.append(card1)
                    black_stack.append(card2)

            # In all other cases both cards can be discarded.

            # Remove two cards from the current stack.
            self.cards = self.cards[:-2]

        # Create a Stack object for each list.
        red_stack = self.__class__(red_stack)
        black_stack = self.__class__(black_stack)

        # Return both Stack objects as a tuple.
        return (red_stack, black_stack)


# -------------------------------- Functions ----------------------------------


# ----------------------------------- Main ------------------------------------
if __name__ == "__main__":
    pass
