from __future__ import annotations
from typing import Union


class Mengenlehreuhr(object):
    def __init__(self, hours, minutes):
        assert 0 <= hours < 24, 'invalid time'
        assert 0 <= minutes < 60, 'invalid time'

        self.minutes = minutes
        self.hours = hours

    def __repr__(self):
        return (self.__class__.__name__ + '(' + str(self.hours) + ', ' +
                str(self.minutes) + ')')

    def __str__(self):
        instructions = self.lamps()

        result = ''

        for index, lamp_row in enumerate(instructions):
            # As long as it is not the third row (index 2), use the standard.
            if index != 2:
                row = ' '
                for i in range(4):
                    if i < lamp_row:
                        row += '#### '
                    else:
                        row += '---- '
                row = row[:-1]

            # On the third row do something special.
            else:
                row = ''
                for i in range(11):
                    if i < lamp_row:
                        row += '# '
                    else:
                        row += '- '
                row = row[:-1]

            result += row + '\n'

        return result[:-1]

    def lamps(self):

        top_row = self.hours // 5
        second_row = self.hours % 5

        third_row = self.minutes // 5
        fourth_row = self.minutes % 5

        return (top_row, second_row, third_row, fourth_row)

    def updateHours(self, hour: int = 1):
        # add the given hours to the current hours (both pos and neg).
        self.hours += hour

        # If the hours end up above 24.
        if self.hours >= 24:
            # Take the module and assign
            self.hours = self.hours % 24

        # If the hours end up negative.
        elif self.hours < 0:
            # Calculate the delta, by taking the module of the absolute value.
            delta = abs(self.hours) % 24
            # Calculate the hours (wrapped around).
            self.hours = 24 - delta
        else:
            # The time is correct
            pass
        # Return a reference.
        return self

    def updateMinutes(self, minute: int = 1) -> Mengenlehreuhr:
        # Add the given minutes to the current minutes (both pos and neg).
        self.minutes += minute

        # If the minutes end up above 60.
        if self.minutes >= 60:
            # Calculate the amount of hours that have to be added.
            delta_hour = (self.minutes // 60)
            # Add the hours.
            self.updateHours(delta_hour)
            # Take the module of the minutes.
            self.minutes = self.minutes % 60

        # If the minutes end up below 0.
        elif self.minutes < 0:
            # Calculate the amount of hours that have to be subtracted.
            # '//' Will preserve the sign.
            delta_hour = (self.minutes // 60)
            # Subtract the hours.
            self.updateHours(delta_hour)
            # Calculate the delta of minutes
            delta = abs(self.minutes) % 60
            # Subtract the delta from 60.
            self.minutes = 60 - delta
        else:
            # The time is correct
            pass

        # Return a reference.
        return self


if __name__ == "__main__":
    Mengenlehreuhr(12, 15)
