class Clock(object):
    def __init__(self, hours: int, minutes: int):
        assert 0 <= hours < 24, 'invalid time'
        assert 0 <= minutes < 60, 'invalid time'

        self.minutes = minutes
        self.hours = hours

        if hours >= 12:
            self.afternoon = True
        else:
            self.afternoon = False

    def __repr__(self):
        return (self.__class__.__name__ + '(' + str(self.hours) + ', ' +
                str(self.minutes) + ')')

    def __str__(self):
        result = ''
        instructions = list(self.lamps())
        colour = instructions[1]
        lights = list(instructions[0])

        first_row = '    '
        if lights[0]:
            first_row += colour
        else:
            first_row += '.'
        result += first_row + '\n'

        second_row = '   '
        if lights[1] == 2:
            second_row += colour + ' ' + colour
        elif lights[1] == 1:
            second_row += colour + ' .'
        else:
            second_row += '. .'
        result += second_row + '\n'

        third_row = '  '
        if lights[2] == 3:
            third_row += colour + ' ' + colour + ' ' + colour
        elif lights[2] == 2:
            third_row += colour + ' ' + colour + ' .'
        elif lights[2] == 1:
            third_row += colour + ' . .'
        else:
            third_row += '. . .'
        result += third_row + '\n'

        fourth_row = ' '
        if lights[3] == 4:
            fourth_row += colour + ' ' + colour + ' ' + colour + ' ' + colour
        elif lights[3] == 3:
            fourth_row += colour + ' ' + colour + ' ' + colour + ' .'
        elif lights[3] == 2:
            fourth_row += colour + ' ' + colour + ' . .'
        elif lights[3] == 1:
            fourth_row += colour + ' . . .'
        else:
            fourth_row += '. . . .'
        result += fourth_row + '\n'

        fifth_row = ''
        if lights[4] == 5:
            fifth_row += colour + ' ' + colour + ' ' + colour + ' ' + colour + ' ' + colour
        elif lights[4] == 4:
            fifth_row += colour + ' ' + colour + ' ' + colour + ' ' + colour + ' .'
        elif lights[4] == 3:
            fifth_row += colour + ' ' + colour + ' ' + colour + ' . .'
        elif lights[4] == 2:
            fifth_row += colour + ' ' + colour + ' . . .'
        elif lights[4] == 1:
            fifth_row += colour + ' . . . .'
        else:
            fifth_row += '. . . . .'
        result += fifth_row

        return result

    def lamps(self):
        result = [[], '']
        if self.afternoon:
            result[1] = 'R'
        else:
            result[1] = 'G'

        hours = self.hours % 12
        minutes = self.minutes

        times = hours * 60 + minutes

        lamps_status = []
        if times >= 360:
            lamps_status.append(1)
            times -= 360
        else:
            lamps_status.append(0)

        if times >= 240:
            lamps_status.append(2)
            times -= 240
        elif times >= 120:
            lamps_status.append(1)
            times -= 120
        else:
            lamps_status.append(0)

        if times >= 90:
            lamps_status.append(3)
            times -= 90
        elif times >= 60:
            lamps_status.append(2)
            times -= 60
        elif times >= 30:
            lamps_status.append(1)
            times -= 30
        else:
            lamps_status.append(0)

        if times >= 24:
            lamps_status.append(4)
            times -= 24
        elif times >= 18:
            lamps_status.append(3)
            times -= 18
        elif times >= 12:
            lamps_status.append(2)
            times -= 12
        elif times >= 6:
            lamps_status.append(1)
            times -= 6
        else:
            lamps_status.append(0)

        if times >= 5:
            lamps_status.append(5)
            times -= 5
        elif times >= 4:
            lamps_status.append(4)
            times -= 4
        elif times >= 3:
            lamps_status.append(3)
            times -= 3
        elif times >= 2:
            lamps_status.append(2)
            times -= 2
        elif times >= 1:
            lamps_status.append(1)
            times -= 1
        else:
            lamps_status.append(0)

        result[0] = tuple(lamps_status)

        return tuple(result)

    def updateHours(self, hours_difference: int = 1):
        if hours_difference >= 0:
            self.hours += hours_difference
            self.hours = self.hours % 24
        else:
            self.hours -= abs(hours_difference)
            if self.hours < 0:
                self.hours = 24 - abs(self.hours)
                while self.hours < 0:
                    self.hours = 24 - abs(self.hours)
            else:
                pass

        if self.hours >= 12:
            self.afternoon = True
        else:
            self.afternoon = False

        return self

    def updateMinutes(self, minutes_difference: int = 1):
        if minutes_difference >= 0:
            self.minutes += minutes_difference
            if self.minutes >= 60:
                extra_hours = self.minutes // 60
                self.updateHours(extra_hours)
                self.minutes = self.minutes % 60
        else:
            self.minutes -= abs(minutes_difference)
            if self.minutes < 0:
                extra_hours = self.minutes // 60
                self.updateHours(extra_hours)
                self.minutes = 60 - abs(self.minutes)
                while self.minutes < 0:
                    self.minutes = 60 - abs(self.minutes)
            else:
                pass
        return self
