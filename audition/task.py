"""
Copyright (C) 2019, [Pieter-Jan Cassiman]

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# --------------------------------- Imports -----------------------------------


# ----------------------------- Global variables ------------------------------

__author__ = "P. Cassiman"
__version__ = '1.0.0'

# --------------------------------- Classes -----------------------------------


class Collection(object):
    """
    Class to deal with collections of integers. All functionality is defined by
    the task description found in the markdown file.
    """

    def __init__(self, coll_desc: list):
        """
        Constructor for the collection object.

        Arguments:
            coll_desc {list} -- A list describing the collection of integers.

        Raises:
            AssertionError: Error raised when the given list contains invalid
                            information
        """

        # Create a list to store the collection of integers.
        self.collection_list = []

        # For each element in the collection description.
        for element in coll_desc:

            # If the element is an integer.
            if isinstance(element, int):
                # Append it the collection list.
                self.collection_list.append(element)

            # If the element is a list, tuple or set.
            elif isinstance(element, (list, tuple, set)):
                # Convert it to a list.
                element = list(element)
                # For each number in the range defined by the element.
                for number in range(element[0], element[1] + 1):
                    # Add this number to the collection list.
                    self.collection_list.append(number)

            else:
                # If the element is not any of the valid types, raise an error.
                raise AssertionError('invalid collection')

        # Remove the duplicates from the list (with the static class method).
        self.collection_list = self.__remove_duplicates(self.collection_list)
        # Sort the list.
        self.collection_list.sort()
        # Return the to rest of the program.
        return

    def __len__(self) -> int:
        """
        This method is called when the len() function is called on the object.

        Returns:
            int -- The amount of integers there are in the collection
        """

        # Return the length of the collection list.
        return len(self.collection_list)

    def __str__(self) -> str:
        """
        Method to return a human readable representation of the object.
        This method uses the output from the normalform() method.

        Returns:
            str -- The human readable respresentation of the object.
        """
        # Return a string representation of the normalform() method.
        return str(self.normalform())

    def __repr__(self) -> str:
        """
        Method to return a verbose representation of the object.
        Returns a string with the classname and the normalform.

        Returns:
            str -- The verbose output.
        """
        # The result string, filled with the class name and the string
        # representation.
        result = '{}({})'.format(self.__class__.__name__, self.__str__())
        # Return the result.
        return result

    def __sub__(self, other):
        """
        Method called when the "-" operator is used on a collection object.
        The result of the method is returned as a new collection object.

        According to the task, this operator is used to find all integers that
        appear in self but not in other.

        Arguments:
            other {Collection} -- The other Collection object.

        Returns:
            Collection -- A new collection with the result.
        """
        # A list for the resulting numbers.
        result_numbers = []

        # For each number in the self collection.
        for number in self.collection_list:
            # If the number is not in the other collection.
            if number not in other.collection_list:
                # Add it to the result list.
                result_numbers.append(number)

        # Return a new collection object with the resulting numbers.
        return self.__class__(result_numbers)

    def __or__(self, other):
        """
        Method called when the "|" operator is used on the collection object.
        The result of the method is returned as a new collection object.

        According to the task, this operator is used to find all integers that
        appear in self or in other.

        Arguments:
            other {Collection} -- The other Collection object.

        Returns:
            Collection -- A new collection with the result.
        """

        # The resulting numbers is the sum of both Collections' data.
        result_numbers = self.collection_list + other.collection_list

        # Return a new collection object with the resulting numbers.
        return self.__class__(result_numbers)

    def __and__(self, other):
        """
        Method called when the "&" operator is used on the collection object.
        The result of the method is returned as a new collection object.

        According to the task, this operator is used to find all integers that
        appear in self and in other.

        Arguments:
            other {Collection} -- The other Collection object.

        Returns:
            Collection -- A new collection with the result.
        """

        # Create a list for the resulting numbers.
        result_numbers = []

        # For each number in self.
        for number in self.collection_list:
            # Check if it is also in other.
            if number in other.collection_list:
                # If it is, add it to the result list.
                result_numbers.append(number)

        # Return a new collection object with the resulting numbers.
        return self.__class__(result_numbers)

    def __xor__(self, other):
        """
        Method called when the "^" operator is used on the collection object.
        The result of the method is returned as a new collection object.

        According to the task, this operator is used to find all integers that
        appear in self or in other, but not in both.

        Arguments:
            other {Collection} -- The other Collection object.

        Returns:
            Collection -- A new collection with the result.
        """

        # Create a list for the resulting numbers.
        result_numbers = []

        # For each number in both the lists.
        for number in (self.collection_list + other.collection_list):
            # If the number is in both collections.
            if ((number not in self.collection_list) or
                    (number not in other.collection_list)):
                # Add it to the resulting list.
                result_numbers.append(number)

        # Return a new collection object with the resulting numbers.
        return self.__class__(result_numbers)

    def __eq__(self, other) -> bool:
        """
        Checks if self is equals to other.

        Arguments:
            other {Collection} -- The Collection object to compare to.

        Returns:
            bool -- wether other is equal to self or not.
        """

        # If the collection list is the same.
        if self.collection_list == other.collection_list:
            # Return True.
            return True
        else:
            # If they are not the same, return False.
            return False

    def __neq__(self, other) -> bool:
        """
        Checks if self is not equals to other.
        returns the inverse result of the __eq__ method.

        Arguments:
            other {Collection} -- The Collection object to compare to.

        Returns:
            bool -- wether other is different from self or not.
        """

        # Return the inverse of the __eq__ method
        return not self.__eq__(other)

    def __lt__(self, other) -> bool:
        """
        This method checks if self is less than other.

        Arguments:
            other {Collection} -- The Collection object to compare to.

        Returns:
            bool -- wether or not self is less than other.
        """

        # Check if other contains at least one more element.
        if len(other) > self.__len__():
            # For each element in the self collection.
            for element in self.collection_list:
                # Check if it present in the other collection.
                if element not in other.collection_list:
                    # If it is not in the collection result False.
                    return False
                else:
                    # If the element is in the other collection continue.
                    continue
            # If each element has been checked, return True.
            return True
        else:
            # if the other collections doesn't contain at least one more
            # integer, return False.
            return False

    def __le__(self, other) -> bool:
        """
        this method checks if self is less than or equal to other.

        Arguments:
            other {Collection} -- The Collection object to compare to.

        Returns:
            bool -- wether or not self is less than, or equal to other.
        """

        # Check if each element of self can be found in other.
        for element in self.collection_list:
            if element not in other.collection_list:
                return False
            else:
                continue
        return True

    def __gt__(self, other) -> bool:
        """
        Checks to see if self is greater than other.

        Arguments:
            other {Collection} -- The Collection object to compare to.

        Returns:
            bool -- wether or not self is greater than other.
        """

        # Check if self contains at least one more integer than other.
        if self.__len__() > len(other):
            # Check if each element of other can be found in self.
            for element in other.collection_list:
                if element not in self.collection_list:
                    return False
                else:
                    continue
            return True
        else:
            return False

    def __ge__(self, other) -> bool:
        """
        Checks to see if self is greater than or equal to other.

        Arguments:
            other {Collection} -- The Collection object to compare to.

        Returns:
            bool -- wether or not self is greater than or equal to other.
        """

        # Check if each element in other can be found in self.
        for element in other.collection_list:
            if element not in self.collection_list:
                return False
            else:
                continue
        return True

    def numbers(self) -> set:
        """
        Method to get a the collection of integers, in long, as a set.

        Returns:
            set -- The set representing the collection of integers.
        """

        # Return the collection of integers as a list.
        return set(self.collection_list)

    def normalform(self) -> list:
        """
        Method to return the normal form of the collection, as described in the
        task assignment.

        All consecutive integers have to be represented as an interval, with
        the beginning and end integers. The other integers have to be
        represented as simple (stand-alone) integers.

        Returns:
            list -- The list representing the normalform of the collection.
        """

        # Start with an empty list, for the result.
        result_list = []
        # Create a copy of the collection list (for safe modifying).
        data_copy = self.collection_list.copy()

        # As long as there are integers in the copied collection.
        while data_copy:
            # Take the first integer.
            first = data_copy[0]

            try:
                # Try to get the second integer.
                second = data_copy[1]
            except:
                # If it fails to get the second integer, it means we are at the
                # end of the collection.
                # Append the "first" integer to the result list.
                result_list.append(first)
                # And return the list (and stop the function call)
                return result_list

            # If the second integer could be determined, calculate the
            # difference, between the second and the first.
            difference = second - first
            # If the difference is one (it is the next possible element in an
            # interval).
            if difference == 1:
                # If the difference is 1, the lower bound of the interval is
                # the first integer.
                lower = first
                # As long as the difference is one, keep going to the next
                # integer in the collection.
                while difference == 1:
                    # Remove the first integer from the collection
                    data_copy = data_copy[1:]
                    # Take a new first integer.
                    first = data_copy[0]
                    try:
                        # Try to take the second integer
                        second = data_copy[1]
                        # Calculate the difference.
                        difference = second - first
                    except:
                        # When the end of the collection is reached break out
                        # of the while loop.
                        break
                    # Keep looping, while the difference is one

                # After the while loop set the upper bound of the interval to
                # the current first integer.
                upper = first
                # Remove the first integer from the list.
                data_copy = data_copy[1:]
                # Add the interval to the result list.
                result_list.append([lower, upper])

            else:
                # If the difference is not one, the integers are not part of an
                # interval.
                # Append the first integer to the result list.
                result_list.append(first)
                # Remove the first integer from the collection.
                data_copy = data_copy[1:]
                # And continue the while loop.

        # After everything return the result.
        return result_list

    @staticmethod
    def __remove_duplicates(data_list: list) -> list:
        """
        Static method to remove duplicates from a list.
        The method goes over each element in the source list and checks if it
        is already in the cleaned list.
        If the element is not yet in the list, it is appended. If the element
        is already in the list, it is ignored.

        Arguments:
            data_list {list} -- The source list, from which the duplicates have
                                to be removed.

        Returns:
            list -- The list, with all duplicates removed.
        """

        # Start with an empty list (for the clean list)
        clean_list = []

        # For each element in the source list.
        for element in data_list:
            # Check if it is not already in the clean list.
            if element not in clean_list:
                # If it is not yet in the clean list, add it.
                clean_list.append(element)

        # Return the clean list.
        return clean_list

# -------------------------------- Functions ----------------------------------


# ----------------------------------- Main ------------------------------------


if __name__ == "__main__":
    pass
