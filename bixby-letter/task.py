"""
The Bixby letter

Copyright (C) 2019, Pieter-Jan Cassiman

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# --------------------------------- Imports -----------------------------------

import math

# ----------------------------- Global variables ------------------------------

__author__ = "P. Cassiman"
__version__ = '0.0.0'

# --------------------------------- Classes -----------------------------------


def cleanse(text: str) -> str:
    """
    Function to clean text according to the following rules:
    - all uppercase letters are replaced with lowercase ones
    - replacing all characters that are no letter with an underscore
    - replacing double underscores with a single one
    - remove leading and trailing underscores.


    Arguments:
        text {str} -- The text to clean.

    Returns:
        str -- The cleaned text, according to the rules.
    """

    # Start by making the text lowercase.
    text_lower = text.lower()

    # Create a variable for the cleaned text
    cleaned = ''

    # Go over each character in the lowercase text.
    for char in text_lower:
        # Check if it's a letter or not.
        if char.isalpha():
            # If it's a letter add it to the cleaned string.
            cleaned += char
        elif char.isnumeric():
            # If it's a number, do nothing.
            pass
        else:
            # In all other cases add an underscore.
            cleaned += '_'

    while '__' in cleaned:
        # Use a while loop, to deal with triple (and more) underscores.
        # Replace double underscores with single ones.
        cleaned = cleaned.replace('__', '_')

    # Remove leading and trailing underscores
    if cleaned[0] == '_':
        cleaned = cleaned[1:]
    if cleaned[-1] == '_':
        cleaned = cleaned[:-1]

    # returned the cleaned text.
    return cleaned


def ngrams(text: str, n: int = 1) -> list:
    """
    Function to create all ngrams of a given text. At first the text is
    cleaned using the cleanse() function.
    The ngrams are returned as a list.

    Arguments:
        text {str} -- The text to split into ngrams.

    Keyword Arguments:
        n {int} -- The length of the ngrams to make. (default: {1})

    Returns:
        list -- The list of all ngrams, created from the given text.
    """

    # TODO: Make function faster, takes really long to deal with large files.

    # Start by cleaning the text.
    text = cleanse(text)

    # Create a list for all ngrams
    pieces_list = []

    # As long as there is text left, keep looping.
    while text:

        # If there are more characters left, than the amount to take.
        if len(text) >= n:
            # Take an ngram from the text.
            piece = text[0:n]
            # Add the ngram to the list.
            pieces_list.append(piece)
            # Remove the first letter from the text.
            text = text[1:]
        else:
            # If there are not enough letters left, we are done and can break
            # out of the loop.
            break

    # Return the list of ngrams.
    return pieces_list


def profile(fileName: str, n: int = 1) -> dict:
    """
    Method to create a dictionary with each ngram and the amount of occurrences
    in the given text file.

    Arguments:
        fileName {str} -- The path to the text file.

    Keyword Arguments:
        n {int} -- The length of the ngrams. (default: {1})

    Returns:
        dict -- The dictionary with the ngrams and the amount of occurrences.
    """

    # Open the text file.
    textFile = open(fileName, 'r', encoding='utf-8')
    # Read from the text file.
    lines = textFile.read()
    # Close the text file.
    textFile.close()

    # Join all the lines together by replace the newline with a dash.
    joined_lines = lines.replace("\n", "-")

    # Create all ngrams in the file.
    ngrams_file = ngrams(joined_lines, n)
    # Create an empty dictionary for the result
    result = {}

    # As long as there are ngrams in the list, keep looping.
    while ngrams_file:
        # Take the first element of the list.
        ngram = ngrams_file[0]
        # Count how many occurrences there are and add it as a key-value pair
        # to the dictionary.
        result[ngram] = ngrams_file.count(ngram)

        # Remove all occurrences of that element from the list.
        # This is not necessarily needed, but speeds up the code.
        ngrams_file[:] = [x for x in ngrams_file if x != ngram]

    # Return the dictionary.
    return result


def ngram_count(dictionary_ngrams: dict) -> int:
    """
    Function to count the amount of ngrams in the dictionary.
    The given dictionary should be one created by the profile function.
    This is done by taking the sum of all the values in the dictionary.

    Arguments:
        dictionary_ngrams {dict} -- The dictionary to count elements in.

    Returns:
        int -- The total amount of ngrams.
    """

    total = 0
    # Take a sum of all the values in the dictionary.
    for value in dictionary_ngrams.values():
        total += value

    return total


def attribution(unknown: dict, known: dict) -> float:
    """
    Method to check if an unknown text can be attributed to an author, by
    comparing the unknown text to a text of the know author.

    The larger the attribution value returned by this function, the greater the
    chance that both text where written by the same author.

    Arguments:
        unknown {dict} -- The unknown text.
        known {dict} -- The text written by a know author.

    Returns:
        float -- The attribution value.
    """

    # Start with a float value to store the attribution value.
    attrib = 0.0

    # For each ngram in the unknown dictionary.
    for ngram in unknown.keys():
        # Use the formula outlined in the task description to calculate the
        # attribution value.
        attrib -= unknown[ngram] * math.log((1 + known[ngram]) /
                                            (ngram_count(known)))

    # Return the attribution value.
    return attrib

# -------------------------------- Functions ----------------------------------


# ----------------------------------- Main ------------------------------------


if __name__ == "__main__":
    bixby = profile('bixby-letter/bixby.txt')
    lincoln = profile('bixby-letter/lincoln.txt')
    hay = profile('bixby-letter/hay.txt')
