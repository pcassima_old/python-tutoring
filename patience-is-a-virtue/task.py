"""
Patience is a virtue

Copyright (C) 2019, Pieter-Jan Cassiman

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# --------------------------------- Imports -----------------------------------

from __future__ import annotations
from typing import Union

# ----------------------------- Global variables ------------------------------

__author__ = "P. Cassiman"
__version__ = '1.0.0'

# --------------------------------- Classes -----------------------------------


class PatienceSorter(object):
    """
    Class for the patience is a virtue task.
    """

    def __init__(self):
        self.stack_list = []

    def stacks(self) -> list:
        """
        Returns the master list (containing all the sub-lists).

        Returns:
            list -- The master list, representing the stacks.
        """
        return self.stack_list

    def stack_count(self) -> int:
        """
        Returns how many sub-lists there are the in the stack.

        For this, the length of the master list is returned.

        Returns:
            int -- The amount of sub-lists.
        """
        return len(self.stack_list)

    def item_count(self) -> int:
        """
        Method to count how many items there are in the stack (total).
        For this the length of each sub-list is summed together and returned.

        Returns:
            int -- The count of how many items there are in the stack.
        """
        # Initialize the counter at 0
        counter = 0

        # For each sub-list in the stack, add the length to the counter.
        for stack in self.stack_list:
            counter += len(stack)

        # Return the counter.
        return counter

    def add_item(self, item: int) -> self:
        """
        Method to add a single item to the stack.
        If the there are no items in the stack yet, the item is added to the
        stack.

        If there are already items in the list, it checks each sub-stack. if
        the new items is smaller than the top one of the sub-stack, the new
        item is added on top of that sub-stack. If it is not smaller, the next
        sub-stack is checked.

        Arguments:
            item {int} -- The new item to add to the stack.

        Returns:
            self -- A reference to the object the method was called on.
        """

        # If there are no items, add the item as a new list.
        if not self.stack_list:
            self.stack_list.append([item])
            # Return self and stop the method.
            return self

        # Go over each sub-list in the stack.
        for i in range(len(self.stack_list)):

            # If the item is smaller or equal to the first item of the sub-list
            # add it to the sub-list, otherwise go on to the next sub-list.
            if item <= self.stack_list[i][0]:
                # Create a list with the item in it.
                new_list = [item]
                # Add the already existing sub-list to the new list.
                new_list += self.stack_list[i]
                # Save the new sub-list back to the stack.
                self.stack_list[i] = new_list
                # Return self.
                return self

        # If all sub-lists have been checked and the item is larger than all the
        # first items, make a new list with the new item.
        self.stack_list.append([item])
        # return self.
        return self

    def add_items(self, items: Union[list, tuple, set]) -> self:
        """
        Method to add a collection of multiple items to the stack.
        Just calls the add_item method for each item in the collection.

        Arguments:
            items {Union[list, tuple, set]} -- The items to be added.

        Returns:
            self -- A reference to the object on which the method was
                    originally called.
        """

        for item in items:
            # For each item in the collection called the add_item method.
            self.add_item(item)

        # Return a reference to the object on which the method was called.
        return self

    def remove_item(self) -> int:
        """
        Method to remove the smallest item from the stack. The smallest item
        is also returned at the end of the function.

        The method will go over the first item in each sub-list and choose the
        smallest

        Raises:
            AssertionError: When the stack is empty and trying to remove an
                            item.

        Returns:
            int -- The smallest item currently in the stack.
        """

        # Check if the list is empty.
        if not self.stack_list:
            raise AssertionError('no more items')

        # Start with both variables at minus one (in this scenario that is
        # impossible, so that's a good start). These values can be used to
        # check if the program is on the first iteration of the loop or not.
        smallest_item = -1
        smallest_index = -1

        # Go over each sub-list in the stack and keep track of the index.
        for i in range(len(self.stack_list)):
            # If we are on the first iteration of the loop
            if smallest_item == -1:
                # The first item is the smallest item.
                smallest_item = self.stack_list[i][0]
                # The current index is the smallest index.
                smallest_index = i

            else:
                # In any other iteration of the loop.
                # Take the first element in the current list as the new
                # possible smallest item.
                possible_smallest = self.stack_list[i][0]

                # If the possible smallest number is smaller than the current
                # smallest item.
                if possible_smallest < smallest_item:
                    # Store it as the newest smallest item.
                    smallest_item = possible_smallest
                    # And store the index.
                    smallest_index = i

                # If it's not the smaller than the current smallest, ignore it.

        # After the smallest item is found, it needs to be removed from the
        # stack.

        # Take the sub-list at the smallest index
        list_to_modify = self.stack_list[smallest_index]

        # If the length of the sublist is larger than one.
        if len(list_to_modify) > 1:
            # Remove the top item from the sub-list (the overall smallest
            # items)
            list_to_modify = list_to_modify[1:]
            # Save the modified list back to the stack copy.
            self.stack_list[smallest_index] = list_to_modify

        else:
            # if the length is 1 or smaller, remove the sub-list.
            self.stack_list.pop(smallest_index)

        # Return the smallest item.
        return smallest_item

    def remove_items(self) -> tuple:
        """
        This method will remove all items from the stacks.
        Starting with the smallest and working up to the largest number.
        After all items have been removed (the stack is empty) and a tuple
        with the items in order is returned.

        Returns:
            tuple -- The tuple with the removed items in order of removal.
        """

        # Start with an empty tuple.
        result = ()

        # For as long as there are items in the stack, keep looping.
        while self.stack_list:
            # Add the result from the remove_item method to the tuple.
            # This is done by added a "one tuple" to the already existing one.
            # The "one tuple" is denoted by (x, ).
            result += (self.remove_item(), )

        # Return the result.
        return tuple(result)


# -------------------------------- Functions ----------------------------------


# ----------------------------------- Main ------------------------------------


if __name__ == "__main__":
    pass
