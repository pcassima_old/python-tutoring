from __future__ import annotations
from typing import Union


class Grid(object):
    def __init__(self, location: Union[str, list, tuple]):
        if isinstance(location, str):
            with open(location, 'r') as inFile:
                contents = inFile.read().rstrip()
                rows = contents.split('\n')
                self.matrix = []
                for row in rows:
                    self.matrix.append([int(number)
                                        for number in row.split(' ')])
        else:
            pass

        self.sorted_rows = False
        self.sorted_columns = False
        self.sorted_rows_decreasing = False
        self.sorted_columns_decreasing = False
        return

    def largest(self) -> int:
        maximum = 0
        for row in self.matrix:
            local_max = max(row)
            maximum = max(local_max, maximum)
        return maximum

    def smallest(self) -> int:
        minimum = -1
        for row in self.matrix:
            local_min = min(row)
            if minimum == -1:
                minimum = local_min
            else:
                minimum = min(local_min, minimum)
        return minimum

    def __str__(self) -> str:
        result = ''
        largest = self.largest()
        if largest <= 9:
            for row in self.matrix:
                for char in row:
                    result += str(char) + ' '
                result = result[:-1] + '\n'
            return result[:-1]

        elif largest <= 99:
            for row in self.matrix:
                for char in row:
                    char = str(char)
                    if len(char) == 1:
                        result += ' ' + char + ' '
                    else:
                        result += char + ' '
                result = result[:-1] + '\n'
            return result[:-1]
        else:
            # Numbers take more digits (not needed in this task)
            raise NotImplementedError('This has to be implemented still')

    def sorted(self, decreasing: bool = False, columns: bool = False) -> bool:
        if decreasing:
            if columns:
                return self.sorted_columns_decreasing
            else:
                return self.sorted_rows_decreasing
        else:
            if columns:
                return self.sorted_columns
            else:
                return self.sorted_rows

    def sort(self, decreasing: bool = False, columns: bool = False) -> self:

        if columns:
            # Sort the columns

            # Transpose the matrix (switch rows and columns)
            t_matrix = self.transpose_matrix(self.matrix)

            # Sort the rows of the transposed matrix (the columns in the
            # original matrix).
            for row_index, _ in enumerate(t_matrix):
                t_matrix[row_index].sort(reverse=decreasing)

            # Transpose the matrix again, to get back the original format.
            self.matrix = self.transpose_matrix(t_matrix)

        else:
            # Sort the rows
            for row_index, _ in enumerate(self.matrix):
                self.matrix[row_index].sort(reverse=decreasing)

        # Return a reference to the object on which the method was called.
        return self

    @staticmethod
    def transpose_matrix(matrix: list) -> list:
        """
        Function to transpose a matrix. This will switch the rows and columns.
        The columns will become the rows and vice versa.

        Array slicing is used for this. This way no extra modules are required,
        such as Numpy (which do the same behind the scenes).

        Arguments:
            matrix {list} -- The matrix to be transposed.

        Returns:
            list -- The transposed matrix.
        """
        return [[matrix[j][i]
                 for j in range(len(matrix))]
                for i in range(len(matrix[0]))]


if __name__ == "__main__":
    pass
