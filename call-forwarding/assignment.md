# Call forwarding
## Preface
Thanks to computer technology the functionality of phone systems has
been greatly enhanced in the last ten years. We now have automated
menus, sophisticated answering machines, conference call capabilities,
group addressing and so on. A common feature of most current phone
systems is the ability to set call forwarding. If somebody goes on
vacation, he sets things up so that all incoming calls are forwarded
to a colleague.

Your task is to write a program that can be used to manage call
forwarding at Ghent University.

All phones at Ghent University have four digit extensions. Employees
can set forwarding by entering the appropriate information through
their telephone interface. If an employee is going to be away he enters
the following information: his extension, the extension his calls
should be forwarded to, the time he will be leaving, and how long he
will be away. This information is subject to the following constraints:

- all phone extensions consist of four digits, where leading zeros are allowed

- the phone extension **9999** is reserved and has special meaning for the call forwarding system (see below)

- times are recorded in increments of one hour and are based on
   a clock that begins at 0 at midnight every New Year's Eve;
   therefore, when describing the time they are leaving, employees
   always use and integer between **0** and **8784**
   (**366 * 24**); the call forwarding system is completely
   reset at the beginning of a year

- call forward set at time **t** for a duration of **d**
  will be in effect from time **t** to time **t+d** inclusive

The call forwarding system may assume that employees enter "correct"
information, in the sense that they follow the formatting rules and
they do not enter a request such that the duration of the request would
go past the end of the year. The system must explicitly check if the
same employee does not enter multiple requests that overlap in time.

But even though employees enter correct, clear, non-overlapping
information from their own point of view, inconsistencies can still
occur in the call forwarding system if requests have been made in such
a way as to forward a call back to the original target of the call. For
example, if Bob forwards his calls to Sue, and Sue forwards her calls
to Joe, and Joe forwards his calls to Bob. When of these three people
their calls would be forwarded forever.

To prevent this situation the call forwarding system uses the dead
end phone extension 9999. Any calls made to an extension involved in
such a degenerate situation will be forwarded to the special phone
extension 9999.

## Assignment

Define a class <i>CallForwarding</i> that can be used to operate a
phone dispatching center that supports call forwarding. The
functionality of the dispatching center must reflect the one described
above. Objects of the class CallForwarding must therefore at least
support the following methods:

- A method setForward that can be used to enter a new request to the
  dispatching center. The method takes four arguments:
  1. the phone extension (str) of the employee that enters the request
  1. the phone extension (str) to which the call must be forwarded to
  1. the time t (int) at which the forward will become in effect
  1. the duration d (int) of the forward. If the employee enters a request
     that overlaps in time with one of his previous requests, the method must
     throw an **AssertionError** with the message "invalid forward".

- A method setForwards that takes the name of a text file as an argument. Each
  line of the text file contains four integers (separated by spaces), where
  each number consists of four digits, where each integer consists of four
  digits. These integers represents the information for entering a call
  forward request: source - target - time - duration. For each request, the
  method must set up a call forwarding from the source to the target at the
  time for a length of duration.

- A method call that can be used to precess incoming calls. This method takes
  two arguments that represent an incoming call to a given phone extension
  (str, first argument) at a given point in time (int, second argument). The
  method must return a string as its result, representing the phone extension
  to which the incoming call will be finally forwarded.
  Forwarding should be done according to the call forwarding requests that
  have been entered into the dispatching center previous to the method call.

## Example
In the following interactive session we can assume the current
directory contains the text file **"settings.txt"**

```python
>>> dispatch = CallForwarding()
>>> dispatch.call('4444', 150)
'4444'
>>> dispatch.setForward('4444', '6666', 100, 200)
>>> dispatch.setForward('5555', '7777', 600, 200)
>>> dispatch.call('5555', 700)
'7777'
>>> dispatch.setForward('5555', '8888', 7400, 100)
Traceback (most recent call last):
AssertionError: invalid forward

>>> dispatch2 = CallForwarding()
>>> dispatch2.setForwards('settings.txt')
>>> dispatch2.call('1111', 50)
'1111'
>>> dispatch2.call('1111', 150)
'2222'
>>> dispatch2.call('1111', 200)
'3333'
>>> dispatch2.call('2222', 225)
'3333'
>>> dispatch2.call('1111', 270)
'9999'
>>> dispatch2.call('1111', 320)
'4444'
>>> dispatch2.call('3333', 320)
'1111'
>>> dispatch2.call('3000', 900)
'3000'
```

