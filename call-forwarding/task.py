"""
Preface:
--------

Thanks to computer technology the functionality of phone systems has been
greatly enhanced in the last ten years. We now have automated menus,
sophisticated answering machines, conference call capabilities, group
addressing and so on. A common feature of most current phone systems is the
ability to set call forwarding. If somebody goes on vacation, he sets things
up so that all incoming calls are forwarded to a colleague.
Your task is to write a program that can be used to manage call forwarding
at Ghent University.

All phones at Ghent University have four digit extensions. Employees can set
forwarding by entering the appropriate information through their telephone
interface. If an employee is going to be away he enters the following
information: his extension, the extension his calls should be forwarded to,
the time he will be leaving, and how long he will be away. This information
is subject to the following constraints:

    - all phone extensions consist of four digits, where leading zeros are
      allowed

    - the phone extension 9999 is reserved and has special meaning for the
      call forwarding system (see below)

    - times are recorded in increments of one hour and are based on a clock
      that begins at 0 at midnight every New Year's Eve; therefore, when
      describing the time they are leaving, employees always use and integer
      between 0 and 8784 (366 * 24); the call forwarding system is
      completely reset at the beginning of a year

    - a call forward set at time t for a duration of d will be in effect
      from time t to time t+d inclusive

The call forwarding system may assume that employees enter "correct"
information, in the sense that they follow the formatting rules and they do
not enter a request such that the duration of the request would go past the
end of the year. The system must explicitly check if the same employee does
not enter multiple requests that overlap in time.
But even though employees enter correct, clear, non-overlapping information
from their own point of view, inconsistencies can still occur in the call
forwarding system if requests have been made in such a way as to forward a
call back to the original target of the call. For example, if Bob forwards
his calls to Sue, and Sue forwards her calls to Joe, and Joe forwards his
calls to Bob. When  of these three people their calls would be forwarded
forever.
To prevent this situation the call forwarding system uses the dead
end phone extension 9999. Any calls made to an extension involved in such a
degenerate situation will be forwarded to the special phone extension 9999.


Assignment:
-----------

Define a class CallForwarding that can be used to operate a phone dispatching
center that supports call forwarding. The functionality of the dispatching
center must reflect the one described above. Objects of the class
CallForwarding must therefore at least support the following methods:

    - A method setForward that can be used to enter a new request to the
      dispatching center. The method takes four arguments:
        - the phone extension (str) of the employee that enters the request
        - the phone extension (str) to which the call must be forwarded to
        - the time t (int) at which the forward will become in effect
        - the duration d (int) of the forward. If the employee enters a
          request that overlaps in time with one of his previous requests,
          the method must throw an AssertionError with the message "invalid
          forward".

    - A method setForwards that takes the name of a text file as an
      argument. Each line of the text file contains four integers (separated
      by spaces), where each number consists of four digits, where each
      integer consists of four digits.
      These integers represents the information for entering a call forward
      request:
          source - target - time - duration.
      For each request, the method must set up a call forwarding from the
      source to the target at the time for a length of duration.

    - A method call that can be used to precess incoming calls. This method
      takes two arguments that represent an incoming call to a given phone
      extension (str, first argument) at a given point in time (int, second
      argument).
      The method must return a string as its result, representing the phone
      extension to which the incoming call will be finally forwarded.
      Forwarding should be done according to the call forwarding requests
      that have been entered into the dispatching center previous to the
      method call.

In the interactive session (on the university server of dodona) we can
assume the current directory contains the text file "settings.txt"


Copyright (C) 2019, Pieter-Jan Cassiman

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# --------------------------------- Imports -----------------------------------


# ----------------------------- Global variables ------------------------------

__author__ = "P. Cassiman"
__version__ = '1.0.0'

# --------------------------------- Classes -----------------------------------


class CallForwarding(object):
    """
    Class for dealing with the call forwarding system. The functionality has
    been specified in the file's docstring.
    """

    def __init__(self):
        self.forwards = []

    def reset(self) -> None:
        """
        This function will reset all forwards, and give a clean slate to start
        with.
        """
        self.forwards = []

    def setForward(self,
                   source: str,
                   target: str,
                   time: int,
                   duration: int) -> None:
        """
        Class method the deal with setting a single forward.

        The function will assure that no more than one forward can be set
        at the same time. Once a number has a forward for a certain period,
        it cannot make a second forward to a different number.
        When this is attempted, a AssertionError is raised.

        Arguments:
            source {str} -- The source of the forwarding.
            target {str} -- To who the call is forwarded
            time {int} -- The time when to start forwarding.
            duration {int} -- How long to forward for.

        Raises:
            AssertionError: When a illegal forward is attempted.
        """

        # Calculate the end of the forward.
        end = time + duration

        # Create a list with existing_forwards of the same source.
        existing_forwards = self.__findExistingForwards(source)

        # Go over all the forwards, starting at the same source.
        for forward in existing_forwards:
            if ((forward[2] <= time <= forward[3]) or
                    (forward[2] <= end <= forward[3])):
                # If the start (or end) time of the forward being entered,
                # is between the start and end time of any of the already
                # entered forwards, the error is raised.
                raise AssertionError("invalid forward")
            else:
                # If the times are right, just do nothing and continue.
                continue

        # When all the times are checked and right (no errors raised), add
        # the forward to the list.
        self.forwards.append((source, target, time, end))

    def setForwards(self, settingFile: str) -> None:
        """
        Class method to set multiple forwards at once. The forwards are read
        from a file and parsed one by one.

        The file contains one line for each forward to be configured. On the
        line are for numbers, separated by a space, each define one parametre
        of the forward (source, target, time, duration).

        Arguments:
            settingFile {str} -- The path to the file with all the forwards
        """

        # Open the file in reading mode.
        file = open(settingFile, 'r')
        # Read the file and split the lines at the same time
        # (split on the newline character).

        # 1) "0001 0002 100 200\n0002 0003 100 200"
        # 2) ["0001 0002 100 200", "0002 0003 100 200"]
        input_forwards = file.read().split("\n")
        # Close the file
        file.close()

        # Go over each line in the input.
        for input_forward in input_forwards:
            # Split off the source, target, time and duration from the line.
            if input_forward:
                source, target, time, duration = input_forward.split(" ")
                # Convert the time to an integer.
                time = int(time)
                # Convert the duration to an integer.
                duration = int(duration)
                # Call the setForward function with the parametres from the
                # file.
                self.setForward(source, target, time, duration)
            else:
                break

    def call(self, target, time, forward_path: list = None) -> str:
        """
        Class method to call an extension. The method will find out if there
        are any forwards for this extension. If there are, it will check the
        times when they are active. If there is a forward active during the
        call time, it will call that extension (recursion).

        However a list will the already tried externsion is also passed to
        call method. This way an infinite loop is avoided. However the list
        has a default value of none, this way the method can be called
        without specifying a list (the initial call).

        Arguments:
            target {str} -- The extension you are trying to call.
            time {int} -- The time at which the call is made.

        Keyword Arguments:
            forward_path {list} -- The list keeping track of the already tried
                                   extensions (default: {None}).

        Returns:
            str -- The extension the call is forwarded too.
        """

        # Initialise the forward target, with the current target.
        # This way when no forward is active at the time of the call, the same
        # extension, as the one called, is returned.
        forward_target = target

        # If the forward_path is not specified create it.
        if not forward_path:
            # Start with the current target.
            forward_path = [target]
        else:
            # If the forward_path is specified, do nothing.
            pass

        # Create a list for the existing forwards to the extension that
        # is being called
        existing_forwards = self.__findExistingForwards(target)

        # Go over all the existing forwards in the list
        for existing_forward in existing_forwards:
            # If there is a forward active at the time of the call
            if (existing_forward[2] <= time <= existing_forward[3]):

                # If the target of the existing (active) forward is already in
                # the list, stop the function and return '9999'.
                if existing_forward[1] in forward_path:
                    return '9999'

                # In all other case's we can continue with forwarding the call.
                else:
                    # Add the new target to the forward_path.
                    forward_path.append(existing_forward[1])
                    # Call the "call" method once again with the new target.
                    forward_target = self.call(existing_forward[1],
                                               time,
                                               forward_path)

        # Return the forward_target.
        return forward_target

    def __findExistingForwards(self, target):
        # Create an empty list for the existing forwards to the extension that
        # is being called
        existing_forwards = []

        # Go over all the forwards in the system.
        for forward in self.forwards:
            # If the forward is for the extension that is being called.
            if forward[0] == target:
                # Add the forward to the list of existing forwards.
                existing_forwards.append(forward)

        # Return the list of existing forwards.
        return existing_forwards


# -------------------------------- Functions ----------------------------------


# ----------------------------------- Main ------------------------------------


if __name__ == "__main__":
    pass
