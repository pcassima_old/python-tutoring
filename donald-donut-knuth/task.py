def isValid(username, name, length=None):
    if length is not None:
        if len(username) < length:
            return False

    name = name.replace(' ', '')

    name = name.lower()
    username = username.lower()

    for char in name:
        try:
            if char == username[0]:
                username = username[1:]
        except IndexError:
            break

    return len(username) == 0


def usernames(names, textfile, length=None):
    result = {}

    usernames = []

    with open(textfile, 'r') as infile:
        for username in infile:
            usernames.append(username.strip())

    # infile = open(textfile, 'r')
    # for username in infile:
    #         usernames.append(username.strip())
    # infile.close()

    for name in names:
        result.update({name: set()})
        for username in usernames:
            if isValid(username, name, length):
                names_temp = result[name]
                names.add(username)
                dict_temp = {name: names_temp}
                result.update(dict_temp)

    return result
