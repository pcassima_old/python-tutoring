"""
Bodyguards

Copyright (C) 2019, Pieter-Jan Cassiman

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# --------------------------------- Imports -----------------------------------


# ----------------------------- Global variables ------------------------------

__author__ = "P. Cassiman"
__version__ = '0.0.0'

# --------------------------------- Classes -----------------------------------


# -------------------------------- Functions ----------------------------------

def find_word(matrix: list) -> str:
    """
    Function to find the word in the given matrix of characters.
    When a character is lowercase and has exactly 3 uppercase neighbours it is
    added to the word. When the entire matrix is parsed the word is returned.
    The matrix does not wrap around, for checking the neighbours.

    Arguments:
        matrix {list} -- The matrix of characters to find the word in.

    Returns:
        str -- The word hidden in the matrix.
    """

    # Start with an empty output string (for building the word).
    word = ''

    # Go over all the rows in the matrix, while keeping track of the index.
    for row_index, _ in enumerate(matrix):
        # Isolate the row at the given index.
        row = matrix[row_index]
        # Go over all the columns (characters) in the row, while keeping track of
        # the index.
        for column_index, _ in enumerate(row):
            # Isolate the character.
            char = row[column_index]

            if char.islower():
                # If the character is lowercase, check and count the
                # neighbours.
                n_guards = 0

                # If the column_index is larger than one (won't wrap around).
                if 1 <= column_index:
                    # Check the left neighbour.
                    nb1 = row[column_index - 1]
                    if nb1.isupper():
                        n_guards += 1

                if column_index + 1 < len(row):
                    # Check the right neighbour.
                    nb2 = row[column_index + 1]
                    if nb2.isupper():
                        n_guards += 1

                if row_index - 1 >= 0:
                    # Check the top neighbour.
                    top_row = matrix[row_index - 1]
                    nb3 = top_row[column_index]
                    if nb3.isupper():
                        n_guards += 1

                if row_index + 1 < len(matrix):
                    # Check the bottom neighbour.
                    bottom_row = matrix[row_index + 1]
                    nb4 = bottom_row[column_index]
                    if nb4.isupper():
                        n_guards += 1

                if n_guards == 3:
                    # If there are exactly 3 correct neighbours, add the
                    # character to the word.
                    word += char
            else:
                continue

    # Finally return the word.
    return word

# ----------------------------------- Main ------------------------------------


# Start with a list for holding the matrix.
rows = []

# Read in how many rows there will be.
n_rows = int(input('Amount of rows: '))

# Read in the specified amount of rows.
for i in range(n_rows):
    rows.append(input('Specify the next row:'))

# Print the result, of the find_word function (the hidden word).
print(find_word(rows))
