def card2symbols(location: str) -> dict:
    with open(location, 'r') as infile:
        result = {}
        for index, line in enumerate(infile.read().strip().split('\n'), start=1):
            symbols = set(line.split(','))
            result[index] = symbols
        return result


def common_symbols(number1: int, number2: int, deck: dict) -> set:
    result = set()
    symbols1 = deck[number1]
    symbols2 = deck[number2]
    for symbol in symbols1:
        if symbol in symbols2:
            result.add(symbol)
    return result


def update_dict(dictionary: dict, key, value) -> dict:

    if key in dictionary.keys():
        value_set = dictionary[key]
        value_set.add(value)
        value = value_set

    else:
        value = {value, }

    return dictionary.update({key: value})


def symbol2cards(deck: dict) -> dict:
    result = {}

    for symbol in deck.keys():
        for value_index, card in enumerate(deck[symbol]):
            update_dict(result, card, symbol)
    return result


def common_cards(symbol1: str, symbol2: str, deck: dict) -> set:
    result = set()
    cards1 = deck[symbol1]
    cards2 = deck[symbol2]
    for card in cards1:
        if card in cards2:
            result.add(card)
    return result

# TODO: write missing cards functions (don't understand the task)


if __name__ == "__main__":
    pass
