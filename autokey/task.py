"""
Autokey

Copyright (C) 2019, Pieter-Jan Cassiman

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# --------------------------------- Imports -----------------------------------


# ----------------------------- Global variables ------------------------------

__author__ = "P. Cassiman"
__version__ = '1.0.0'

# --------------------------------- Classes -----------------------------------


# -------------------------------- Functions ----------------------------------

def substitution_matrix() -> list:
    """
    Function to create the substitution matrix used in this task.
    Will create a list of 26 sub-lists, each containing the 26 letters of the
    alphabet. Each sub-list (or row in the matrix) has the letters shifted one
    place to the left, when compared to the previous row.

    Returns:
        list: The substitution matrix.
    """

    # Start with an empty matrix.
    matrix = []
    # Loop 26 times, once for each row.
    for i in range(26):
        # Create a row.
        row = []
        # Loop 26 times, once for each column.
        for j in range(26):
            # Create the character to go in that place.
            # The i-index is used to offset each row, while the j-index is used
            # to make the letters. The module 26 is used to make sure the
            # characters remain inside the alphabet.
            char = chr((i + j) % 26 + 65)
            # Add the character to the row.
            row.append(char)
        # Add the row to the matrix.
        matrix.append(row)
    # Return the matrix.
    return matrix


def substitute(plain: str, key: str) -> str:
    """
    Function to substitute (or replace), a single character in the plaintext.
    For this one character from the keyword is used.
    The character from the key represents the row_index and the caracter from
    the plaintext represents the column_index. This is also how the cipher
    character is found in the matrix.

    Args:
        plain (str): A character from the plaintext.
        key (str): A character from the keyword (same index as plain).

    Returns:
        str: The substitution character.
    """

    # Use the plain character to find the column index.
    column_index = ord(plain.upper()) - 65
    # Use the key character to find the row index.
    row_index = ord(key.upper()) - 65

    # Use the row index to find the row.
    row = substitution_matrix()[row_index]
    # Use the column index to find the substitution character.
    char = row[column_index]

    # Return the character.
    return char


def clean_message(message: str) -> str:
    """
    Function to clean a message.
    This function will remove all characters that are not letters.
    It will also make all the characters uppercase.

    Args:
        message (str): The message to be cleaned.

    Returns:
        str: The cleaned message.
    """

    # Start with an empty string for the cleaned message.
    cleaned_message = ''

    for char in message:
        # Loop over each character in the message to be cleaned.
        if char.isalpha():
            # Check if the character is a letter, if it is add it the
            # cleaned_message, but also make it upper case.
            cleaned_message += char.upper()

    # Return the cleaned message.
    return cleaned_message


def encode(message: str, key: str) -> str:
    """
    Function to encode a message with the given key.
    The function will go over each character and key, to encode the message.
    For this the substitution matrix is used.

    Args:
        message (str): The message to encode.
        key (str): The key to encode the message with.

    Returns:
        str: The encoded message.
    """

    # Clean message and key.
    message = clean_message(message)
    key = clean_message(key)

    # Create message with the key in the front (the create the keyword).
    keyword = key + message
    # Create an empty string to store the encoded message.
    ciphertext = ''
    # Loop over all the character in the message and keep track of the index.
    for index, plain_char in enumerate(message):
        # Extract a character from the keyword.
        key_char = keyword[index]
        # Use the character from the plaintext and keyword to encode a
        # character in the ciphertext.
        ciphertext += substitute(plain_char, key_char)

    # Return the ciphertext.
    return ciphertext


def decode(ciphertext: str, key: str) -> str:
    """
    Function to decode a ciphertext with the given text.
    The characters from the key are used to decode the ciphertext.
    As characters are decoded they are added to the key, so the loop won't
    "run out of key", to further decode.

    Args:
        ciphertext (str): The text to decode.
        key (str): The key that was used to encode the message.

    Returns:
        str: The decoded message.
    """

    # Start with a string to store the decoded message.
    plaintext = ''

    # Use the function to create a substitution matrix.
    sub_matrix = substitution_matrix()

    # Clean the key.
    key = clean_message(key)

    # Loop as long as there are characters left to be decoded.
    while ciphertext:

        # The first character of the key represents the row index in the matrix.
        row_char = key[0]
        row_index = ord(row_char) - 65

        # Remove the first character from the key, as it was used.
        key = key[1:]

        # Use the row index to take out the row from the matrix.
        row = sub_matrix[row_index]

        # Take the first character from the cipher, as it represents the
        # column index.
        cipher_char = ciphertext[0]

        # remove the first character from the ciphertext, as it is decoded.
        ciphertext = ciphertext[1:]

        for column_index, column_char in enumerate(row):
            # Loop over all the characters in the row, while keeping track of
            # the index.
            if cipher_char == column_char:
                # If the character is the same as the first character in the
                # ciphertext, use the column index to get the original
                # character.
                plain_char = chr(column_index + 65)
                # Add the character to the plain text.
                plaintext += plain_char
                # Also add the character to the key.
                key += plain_char
                # Stop the for loop.
                break

    # Return the decoded message.
    return plaintext

# ----------------------------------- Main ------------------------------------


if __name__ == "__main__":
    pass
