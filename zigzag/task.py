"""
ZigZag

Copyright (C) 2019, Pieter-Jan Cassiman

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# --------------------------------- Imports -----------------------------------

from typing import Union

# ----------------------------- Global variables ------------------------------

__author__ = "P. Cassiman"
__version__ = '1.0.0'

# --------------------------------- Classes -----------------------------------


# -------------------------------- Functions ----------------------------------

def isZigzag(sequence: Union[list, tuple]) -> bool:
    """
    This function will check if the given sequence is sorted according to the
    zigzag pattern.

    Will return True if the sequence is correctly sorted and False if not.

    Arguments:
        sequence {Union[list, tuple]} -- The sequence to check.

    Returns:
        bool -- Wether or not the sequence is correctly sorted or not.
    """

    # Loop over the elements in the list and keep track of the index.
    for index, element in enumerate(sequence):

        if index > 0:
            # If the index is larger that zero, take the previous element.
            previous_element = sequence[index - 1]
        else:
            previous_element = None

        if index < len(sequence) - 1:
            # If the index is less than the length of the sequence take the
            # next element.
            next_element = sequence[index + 1]
        else:
            next_element = None

        if index % 2:
            # If index is odd, check that next and previous are smaller.
            if previous_element and not previous_element >= element:
                return False
            if next_element and not next_element >= element:
                return False

        else:
            # if index is even, check that next and previous are larger.
            if previous_element and not previous_element <= element:
                return False
            if next_element and not next_element <= element:
                return False

    return True


def zigzagSlow(sequence: Union[list, tuple]) -> None:
    """
    Will sort the given sequence according to the slow way, i.e.:
    The list is sorted in ascending order and then each pair of elements is
    swapped.

    Arguments:
        sequence {Union[list, tuple]} -- The sequence to sort.

    Returns:
        None -- Nothing is returned.
    """

    # Start by sorting the sequence.
    sequence.sort()

    # Loop of the list, with an index, but increase it by two each time.
    for i in range(0, len(sequence), 2):
        try:
            # Try to swap the current index with the next one.
            temp = sequence[i]
            sequence[i] = sequence[i + 1]
            sequence[i + 1] = temp
        except IndexError:
            # If the index goes out of range, the end of the sequence is
            # reached, and this can be ignored.
            pass


def zigzagFast(sequence: Union[list, tuple]) -> None:
    """
    Will sort the given sequence according to the fast way. For this all that
    is requires is to traverse the even positions from left to right, and on
    each position execute the following two steps one after the other:

    1. if the current element at the even position is less than the element on
       the previous odd position (if that position exists), swap both elements

    2. if the current element at the even position is less than the element on
       the next odd position (if that position exists), swap both elements

    Arguments:
        sequence {Union[list, tuple]} -- The sequence to sort.

    Returns:
        None -- Nothing is returned.
    """
    # Loop over the sequence, increasing the index by two each time;
    # this will loop over all elements at the even spots.
    for i in range(0, len(sequence), 2):
        try:
            if i > 0:
                # If i is larger than zero, take the previous element.
                current = sequence[i]

                if current < sequence[i - 1]:
                    # If the previous element is larger, swap.
                    sequence[i] = sequence[i - 1]
                    sequence[i - 1] = current

            if i < len(sequence) - 1:
                # If i is smaller than the length of the sequence, take the
                # next element.
                current = sequence[i]
                if current < sequence[i + 1]:
                    # If the next element is larger, swap.
                    sequence[i] = sequence[i + 1]
                    sequence[i + 1] = current
        except IndexError:
            # Ignore index errors, the end of the sequence has been reached at
            # this point.
            pass

# ----------------------------------- Main ------------------------------------


if __name__ == "__main__":
    pass
