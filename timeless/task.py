"""
Timeless

Copyright (C) 2019, Pieter-Jan Cassiman

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# --------------------------------- Imports -----------------------------------

from datetime import date

# ----------------------------- Global variables ------------------------------

__author__ = "P. Cassiman"
__version__ = '0.0.0'

weekdays = {
    1: 'Sun',
    2: 'Mon',
    3: 'Tue',
    4: 'Wed',
    5: 'Thu',
    6: 'Fri',
    7: 'Sat',
}

# --------------------------------- Classes -----------------------------------


# -------------------------------- Functions ----------------------------------

def compare_days(one: date, other: date) -> bool:
    """
    Function to check if two days fall on the same weekday.

    Arguments:
        one {date} -- The first date.
        other {date} -- The second date.

    Returns:
        bool -- Wether or not the two dates fall on the same weekday.
    """

    return one.weekday() == other.weekday()


def first_difference(year1: int, year2: int) -> date:
    """
    This function will find and return the first date that falls on different
    weekdays, in both given years.

    Will return None if there are no different dates.

    Arguments:
        year1 {int} -- The year to check against.
        year2 {int} -- The year to find the difference in.

    Returns:
        date -- The date that is different between the two years.
    """

    for month in range(1, 13):
        # Go over all the months.
        for day in range(1, 32):
            # Go over all the days.
            try:
                # Try to make the date
                date1 = date(year1, month, day)
                date2 = date(year2, month, day)
                if not compare_days(date1, date2):
                    return date2
            except ValueError:
                # If it fails to make the date.
                if day == 29:
                    # If the day is the 29th, we have to deal with February.
                    try:
                        # Try to make a day with the 29th of February.
                        # Because the try failed on the first date creation.
                        return date(year2, 2, 29)
                    except ValueError:
                        # If it fails to create a second date on the 29th of
                        # February, it can be ignored and the rest of the days
                        # can be checked.
                        pass
                else:
                    # in most cases this can be ignored and just go the the
                    # next month.
                    break

    # If both loops finish, no different date has been found, so None has to be
    # returned.
    return None


def reuse_calendar(current: int,
                   previous: bool = False,
                   start: int = None) -> int:
    """
    Function returns the first calendar that can be reused for the current
    (given) year. The function will check the future years by default. By
    specifying previous as True, the function will check the past years.

    Arguments:
        current {int} -- The current year

    Keyword Arguments:
        previous {bool} -- Wether or not to check past years (default: {False})
        start {int} -- The value at which to start checking (default: {None})

    Returns:
        int -- The first year of which the calendar can be reused,
    """

    # previous is True, check backwards.
    if previous:
        # Start with a new year (one less than the current year).
        if start:
            # If a start value is specified, use that to start.
            new = start - 1
        else:
            new = current - 1
        while first_difference(current, new) is not None:
            # Keep looping and decrementing the years until a year with no
            # different days is found.
            new -= 1
        return new

    # Otherwise check forwards.
    else:
        # Start with a new year (one more than the current year).
        if start:
            # If a start value is specified, use that to start.
            new = start + 1
        else:
            new = current + 1
        while first_difference(current, new) is not None:
            # Keep looping and incrementing the years until a year with no
            # different days is found.
            new += 1
        return new


def reuse_calendars(current: int, amount: int, previous: bool = False):
    """
    Function will find to first x-amount years of which the calendars can be
    reused for the current year.

    The function can check both future and past years.

    Arguments:
        current {int} -- The current year.
        amount {int} -- How many calendars have to be found.

    Keyword Arguments:
        previous {bool} -- Wether or not to check past years (default: {False})

    Returns:
        list -- A list of years of which the calendars can be reused.
    """

    # Start with an empty list.
    calenders = []
    # As long as we don't have the required amount of years, keep looping.
    while len(calenders) != amount:

        if calenders:
            # If there is something in the list, use it as the start value.
            # The next year will then be found.
            calenders.append(reuse_calendar(current, previous, calenders[-1]))
        else:
            # Otherwise just find the first year of which the calendar can be
            # reused.
            calenders.append(reuse_calendar(current, previous))

    return calenders

# ----------------------------------- Main ------------------------------------


if __name__ == "__main__":
    pass
