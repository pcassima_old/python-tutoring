"""
Geohash-36

Copyright (C) 2019, Pieter-Jan Cassiman

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# --------------------------------- Imports -----------------------------------

# ----------------------------- Global variables ------------------------------

__author__ = "P. Cassiman"
__version__ = '0.0.0'

# --------------------------------- Classes -----------------------------------


class Geohash36(object):

    default_alphabet = {
        '2': 0, '3': 1, '4': 2, '5': 3, '6': 4, '7': 5,
        '8': 6, '9': 7, 'b': 8, 'B': 9, 'C': 10, 'd': 11,
        'D': 12, 'F': 13, 'g': 14, 'G': 15, 'h': 16, 'H': 17,
        'j': 18, 'J': 19, 'K': 20, 'l': 21, 'L': 22, 'M': 23,
        'n': 24, 'N': 25, 'P': 26, 'q': 27, 'Q': 28, 'r': 29,
        'R': 30, 't': 31, 'T': 32, 'V': 33, 'W': 34, 'X': 35,
    }

    def __init__(self, code: str, alphabet: str = None):
        """
        Initialiser for the geohash36 object.

        Arguments:
            code {str} -- The code for the geohash.

        Keyword Arguments:
            alphabet {str} -- The alphabet used to encode the geohash.
                              Should be 36 unique alphanumeric characters.
                              If None is specified, the default is used.
                              (default: {None})

        Raises:
            AssertionError: Invalid alphabet (if an other amount than 36
                            characters are specified).
            AssertionError: Invalid alphabet (if there are no 36 unique
                            characters).
            AssertionError: Invalid code (if the code contains characters not
                            in the alphabet).
            AssertionError: Invalid code (if the checksum doesn't match).
        """

        # -------- Alphabet --------
        if alphabet is None:
            # If no alphabet is specified, use the default alphabet.
            self.default_alphabet_used = True
            self.alphabet = self.default_alphabet

        else:
            # If a custom alphabet is specified.

            if len(alphabet) != 36:
                # If any other amount of characters is defined, raise an error.
                raise AssertionError('invalid alphabet')

            else:
                # If the correct amount of characters is defined, continue.
                self.default_alphabet_used = False
                self.alphabet = {}

                # Go over each character in the specified alphabet and keep
                # track of a counter (for the value).
                counter = 0
                for char in alphabet:

                    if char not in self.alphabet.keys() and char.isalnum():
                        # If the character meets the rules, add it to the
                        # alphabet, with the correct value (counter).
                        self.alphabet[char] = counter
                        # Increment the counter.
                        counter += 1

                    else:
                        # If the character, doesn't meet the rules throw an
                        # error.
                        raise AssertionError('invalid alphabet')

        # -------- Matrix --------

        # Start with an empty matrix list.
        matrix = []

        # Create a list of all the keys in the alphabet.
        alphabet_keys = list(self.alphabet.keys())

        # Create a 6x6 matrix (representing the area).
        for row in range(6):
            sub_list = []
            for column in range(6):
                sub_list.append(alphabet_keys[column + (6 * row)])
            matrix.append(sub_list)

        # Reverse the matrix (required by the text).
        matrix.reverse()

        # Save the matrix, to the object.
        self.matrix = matrix

        # -------- Code & checksum --------
        # If the code contains a dash, it has a checksum.
        if '-' in code:
            # The last character is a checksum.
            checksum = code[-1]
            # Remove the checksum and dash.
            code = code[:-2]

        else:
            # Set the checksum to nothing.
            checksum = ''

        for char in code:
            # Check if all characters are in the alphabet.
            if char not in self.alphabet.keys():
                # Throw an error if a character is not in the code.
                raise AssertionError('invalid code')

        #  Save the code to the object.
        self.code = code

        if checksum:
            # If there is a checksum, check it with the calculated value.
            if checksum != self.checksum():
                # If the two values don't match raise an error.
                raise AssertionError('invalid code')

    def __str__(self) -> str:
        """
        Returns a string representation of the code with the checksum.

        Returns:
            {str} -- The string representation of the code.
        """
        # Add the code to the string.
        result = str(self.code)
        # Add a dash.
        result += '-'
        # Add the checksum.
        result += self.checksum()
        # Return the string.
        return result

    def __repr__(self):
        result = str(self.__class__.__name__)
        result += "('"
        result += self.__str__()
        result += "'"

        if self.default_alphabet_used:
            result += ')'
            return result
        else:
            result += ", alphabet='"
            for char in self.alphabet.keys():
                result += char
            result += "')"
            return result

    def checksum(self):
        checksum_value = 0

        code_copy = self.code[::-1]
        for index in range(len(code_copy)):
            char = code_copy[index]
            checksum_value += self.alphabet[char] * (index + 1)

        checksum_value = checksum_value % 26

        result = chr(checksum_value + 97)

        return result

    def position(self, t: str):

        result_row_index = 0
        result_column_index = 0
        for row_index in range(len(self.matrix)):
            row = self.matrix[row_index]
            if t in row:
                result_row_index = row_index
                for column_index in range(len(row)):
                    column = row[column_index]
                    if t == column:
                        result_column_index = column_index
                        return result_row_index, result_column_index

        raise AssertionError('invalid symbol')

    def longitude(self):

        lower = -180
        upper = 180
        for char in self.code:
            _, column_index = self.position(char)

            longitude_range = upper - lower

            section_range = longitude_range / 6

            sections = []

            for i in range(6):
                sections.append([lower + (section_range * i),
                                 lower + (section_range * (i + 1))])

            right_section = sections[column_index]

            lower = right_section[0]
            upper = right_section[1]

        return lower, upper

    def latitude(self):

        lower = -90
        upper = 90
        for char in self.code:
            row_index, _ = self.position(char)

            longitude_range = upper - lower

            section_range = longitude_range / 6

            sections = []

            for i in range(6):
                sections.append([lower + (section_range * i),
                                 lower + (section_range * (i + 1))])

            right_section = sections[row_index]

            lower = right_section[0]
            upper = right_section[1]

        return lower, upper

    def coordinates(self):

        lower_long, upper_long = self.longitude()

        lower_lat, upper_lat = self.latitude()

        longitude = ((upper_long - lower_long) / 2) + lower_long
        latitude = ((upper_lat - lower_lat) / 2) + lower_lat

        return longitude, latitude

# -------------------------------- Functions ----------------------------------

# ----------------------------------- Main ------------------------------------


if __name__ == "__main__":
    pass
