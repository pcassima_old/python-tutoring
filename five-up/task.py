"""
Five up

Copyright (C) 2019, Pieter-Jan Cassiman

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# --------------------------------- Imports -----------------------------------

from typing import Union

# ----------------------------- Global variables ------------------------------

__author__ = "P. Cassiman"
__version__ = '0.0.0'

# Lists to store the accepted values and suits of the cards.
values = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K']
suits = ['C', 'D', 'H', 'S']

# --------------------------------- Classes -----------------------------------


# -------------------------------- Functions ----------------------------------

def is_valid_card(card: str) -> bool:
    """
    Function to test if a given card is a valid card.
    Meaning it complies to the following rules:
    - both the value and suit are correct
    - both the value and suit are either upper- or lowercase, they cannot be
      mixed

    Args:
        card (str): The card to check.

    Returns:
        bool: Wether or not the card is valid; True if valid.
    """

    # Check if the card is a string.
    if isinstance(card, str):
        # Isolate the value and suit.
        card_value = card[:-1]
        card_suit = card[-1]

        # Check if the both the value and suit have the same case (either upper
        # or lowercase) or if the value is a number.
        if (card_value.isnumeric() or
                card_value.isupper() == card_suit.isupper()):

            # Return true if both the suit and value can be found in their
            # respective lists.
            return card_suit.upper() in suits and card_value.upper() in values

    # In all other cases return False.
    return False


def is_valid_packet(packet: list) -> bool:
    """
    Method to check if all cards in the given packet are valid.
    For this the any keyword and a lambda function is used.

    Args:
        packet (list): The packet to check the cards in.

    Returns:
        bool: True if the packet contains only valid cards.
    """

    # Use a lambda-function and the any keyword to check if all the cards in
    # the packet are valid.
    if any(not card for card in map(lambda card: is_valid_card(card), packet)):
        return False
    else:
        return True


def string_representation(packet: list) -> str:
    """
    Function to create the string representation of the a given packet.
    Each card is checked, to see if it is a valid card.
    If it is a valid card it is eithe represented as the card itself (face up)
    or as '**' (face down).

    Args:
        packet (list): The packet to represent.

    Raises:
        AssertionError: If the packet contains an invalid card.

    Returns:
        str: The string representation of the the packet.
    """

    # Star with an empty string for the result.
    result = ''
    # Loop over all the cards in the packet.
    for card in packet:
        # Check if it is a valid card.
        if is_valid_card(card):
            # Check if the suit is lower case (to check if the card is
            # face down).
            if card[-1].islower():
                result += '**'
            else:
                result += card

            # Add a space to separate the cards.
            result += ' '

        else:
            # Raise an error of there is an invalid card.
            raise AssertionError('invalid packet')

    # Return the result, minus the trailing space.
    return result[:-1]


def turn_selection(packet: list, sequence: Union[list, tuple] = None) -> list:
    """
    Function to turn cards in the pack. If no sequence is defined all cards are
    turned over. Otherwise the cards at the defined positions.

    Args:
        packet (list): The packet of cards, where cards have to be turned over.
        sequence (Union[list, tuple], optional): A sequence of cards to flip,
                if not specified all cards are flipped. Defaults to None.

    Raises:
        AssertionError: When the packet contains an invalid card.

    Returns:
        list: The packet with the required cards flipped.
    """

    # Check if the packet is valid.
    if not is_valid_packet(packet):
        raise AssertionError('invalid packet')

    # If no sequence is defined, create one for all the places in the packet.
    if sequence is None:
        sequence = [index + 1 for index, _ in enumerate(packet)]

    # Go over all the positions in the sequence.
    # The positions, start from one, while the index starts from 0.
    # Thats why all the positions have '-1'
    for position in sequence:
        # Take the card at that position.
        card = packet[position - 1]

        # Flip the card at the position. The card flipping happens in place.
        if card[-1].islower():
            packet[position-1] = card.upper()
        else:
            packet[position-1] = card.lower()

    # Return the packet.
    return packet


def turn_top(packet: list, number: int) -> None:
    """
    Function to turn the top number of cards. The operation happens in place,
    meaning that the packet is modyfied outside of the function.

    Args:
        packet (list): The master packet.
        number (int): How many cards from the top to turn.

    Raises:
        AssertionError: Invalid packet

    Returns:
        list: The packet with the cards turned over.
    """

    # Check if all cards, in the pack, are valid.
    if not is_valid_packet(packet):
        raise AssertionError('invalid packet')

    # Take the top amount of cards.
    top_cards = packet[:number]
    # Reverse the top cards.
    top_cards.reverse()
    # Flip all the top cards.
    top_cards = turn_selection(top_cards)

    # Modify the packet list in place (will be modyfied outside of the
    # function).
    for index, turned_card in enumerate(top_cards):
        packet[index] = turned_card

    # Return the modyfied packet.
    return packet


def cut(packet: list, number: int) -> list:
    """
    Function to cut a packet into two smaller packets. The split happens at the
    position indicated by the number; In other words the top packet will
    contain a [number] amount of cards.

    The top sub-packet is placed on the bottom of the second sub-packet. This
    way a new packet is made. All modifications happen in-place.

    Args:
        packet (list): The packet to cut and rearrange.
        number (int): The position at which to cut the packet.

    Raises:
        AssertionError: invalid stack.

    Returns:
        list: The modyfied packet.
    """

    # Check if all cards, in the pack, are valid.
    if not is_valid_packet(packet):
        raise AssertionError('invalid packet')

    # Define a function to shift the cards over one spot to the left;
    # Or cut the top card and place it on the bottom.
    def shift(times):
        # If times is 0, the function is done shifting cards around.
        if times == 0:
            return packet

        # In other cases shift cards around.
        else:
            # Temporarily store the first (top) card.
            temp = packet[0]
            # For each card in the packet, move it one position to the left.
            for index in range(len(packet) - 1):
                packet[index] = packet[index + 1]

            # Set the last position (bottom) to the card we stored.
            packet[index + 1] = temp

            # Call the function again, but with shifting one less.
            return shift(times - 1)

    # Return the result of the shift function, with x cards moved from the top
    # to the bottom.
    return shift(number)

# ----------------------------------- Main ------------------------------------


if __name__ == "__main__":
    pass
