"""
Colony picker

Copyright (C) 2019, Pieter-Jan Cassiman

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# --------------------------------- Imports -----------------------------------


# ----------------------------- Global variables ------------------------------

__author__ = "P. Cassiman"
__version__ = '0.0.0'

# --------------------------------- Classes -----------------------------------


class PetriDish(object):
    def __init__(self, textFile: str):

        banana = open(textFile, 'r')

        image = banana.read()

        banana.close()

        image_lines_temp = image.split("\n")

        self.image_lines = []
        for line in image_lines_temp:
            if line:
                self.image_lines.append(line)

        self.image_lines = self.image_lines[1:]

    def __str__(self):
        # Print out the lines of the bitmap.
        result = ""
        for line in self.image_lines:
            result += "\n"
            result += line

        return result

    def colony(self, row: int, column: int):
        # A way of checking wether the position given is in a colony or not.
        raise AssertionError("no colony found on position ({}, {})".format(
            row, column))
        # A way of finding neighbours or one connected area.
        # Check the row you are one for neighbours.
        # Check if any of the row-neighbours has any top or bottom neighbours
        # Repeat until no new neighbours are found.

    def undo(self):
        # Convert all periods back to hashes
        pass

    def count(self, minimum: int = 1):
        pass

    def size(self, minimum: int = 1):
        pass


# -------------------------------- Functions ----------------------------------


# ----------------------------------- Main ------------------------------------


if __name__ == "__main__":
    pass
