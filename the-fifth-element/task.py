"""
The fifth element.

Copyright (C) 2019, Pieter-Jan Cassiman

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# --------------------------------- Imports -----------------------------------

from __future__ import annotations
import math

# ----------------------------- Global variables ------------------------------

__author__ = "P. Cassiman"
__version__ = '1.0.0'

# --------------------------------- Classes -----------------------------------


class Grid:
    def __init__(self, integer_list: list):
        """
        Initialiser for the Grid class.
        Will build a matrix from the given integer list. At the same time a
        second matrix is built, storing the states of the numbers in the
        matrix. These states are initialised as 0 (meaning the numbers are
        available). This state is used other methods and later in the task.

        The matrix will always be a square matrix in this case.

        Arguments:
            integer_list {list} -- A list describing the elements in the
                                   matrix.
        """

        # Take the square root of the length of the list.
        square = math.sqrt(len(integer_list))
        # If the square root is an integer, the list contains enough items to
        # fill a square matrix.
        assert square.is_integer(), 'invalid elements'
        # Convert to square root to an integer.
        square = int(square)

        # Check if all element in the given list are unique elements and
        # integers.
        for i in integer_list:
            assert (integer_list.count(i) == 1 and
                    isinstance(i, int)), 'invalid elements'

        # Create a list for storing the matrix and the states.
        matrix = []
        states = []

        # Loop a square-amount of times.
        for i in range(square):
            # Make a row for the matrix and the states.
            row = []
            states_row = []
            # Loop a square-amount of times.
            for j in range(square):
                # Add the number from the given list to the row.
                # 'j' is used as an index, while 'i' is used as an offset.
                row.append(integer_list[j + (i * square)])
                # Add all zero's to the list, for initialising the states.
                states_row.append(0)
            # Append the rows to the matrices.
            matrix.append(row)
            states.append(states_row)

        # Store the matrices to the object, being created.
        self.matrix = matrix
        self.states = states

    def __str__(self):
        result = ''
        size = len(self.matrix[0])
        for row_index, row in enumerate(self.matrix):
            for column_index, column in enumerate(row):
                if self.states[row_index][column_index] == 2:
                    if size > 9:
                        result += '  -'
                    elif size > 3:
                        result += ' -'
                    else:
                        result += '-'
                else:
                    if size > 9:
                        if column > 99:
                            result += str(column)
                        elif column > 9:
                            result += ' ' + str(column)
                        else:
                            result += '  ' + str(column)
                    elif size > 3:
                        if column > 9:
                            result += str(column)
                        else:
                            result += ' ' + str(column)
                    else:
                        result += str(column)
                result += ' '
            result = result[:-1] + '\n'
        return result[:-1]

    def element(self, row_index: int, column_index: int) -> int:
        """
        Method to return the element at the given row and column

        Arguments:
            row_index {int} -- The row index of the element.
            column_index {int} -- The column index of the element.

        Raises:
            AssertionError: When the given indices are out of range for the
                            matrix.

        Returns:
            int -- The element at the given location.
        """

        # Check that both indices are larger than zero.
        assert row_index >= 0, 'invalid position'
        assert column_index >= 0, 'invalid position'

        try:
            # Try to get the element from the matrix.
            row = self.matrix[row_index]
            ele = row[column_index]
            # If the element can be found, return it.
            return ele
        except IndexError:
            # If an IndexError is raised, catch it and raise an AssertionError.
            # An AssertionError is required by the task.
            raise AssertionError('invalid position')

    def position(self, element: int) -> tuple:
        """
        Method to find the given element in the matrix.

        Arguments:
            element {int} -- The element to find.

        Raises:
            AssertionError: When the element is not in the matrix.

        Returns:
            tuple -- A tuple containing the row- and column index of the
                     element.
        """

        # Loop over each row in the matrix and keeping track of the index.
        for row_index, row in enumerate(self.matrix):
            # Check if the element is in the row (to make the method faster).
            if element in row:
                # Loop over each column in the row while keeping track of the
                # index.
                for column_index, column in enumerate(row):
                    if column == element:
                        # If the element is in the column return the current
                        # row and column index.
                        return (row_index, column_index)

        # If the element is not found after going over the entire matrix, raise
        # an error.
        raise AssertionError('element not found')

    def select(self, element: int) -> self:
        """
        This method will select an element in the matrix, and cross out the
        other elements in the column & row of the selected element.
        For this the appropriate value is given to the same location in the
        states matrix.

        The following values ares used in the [states] matrix:
        0: available to select
        1: Selected
        2: unavailable to select (crossed out)

        By using a second matrix to hold the state, the original matrix is
        preserved, while also keeping it simple to parse the matrices.

        Arguments:
            element {int} -- The element that has to be selected.

        Raises:
            AssertionError: When the element is not in the matrix.

        Returns:
            self -- A reference to the object, the method was called on.
        """

        # Find the position of the element in the matrix.
        row_index, column_index = self.position(element)

        # Check that the element not yet crossed out, or selected.
        # i.e. it is available to select.
        if self.states[row_index][column_index] != 0:
            raise AssertionError('invalid selection')

        # If the element is available.
        else:
            # Set all elements in row to state 2
            for column_index_2, _ in enumerate(self.states[row_index]):
                self.states[row_index][column_index_2] = 2

            # Set all elements in column to state 2
            for row_index_2, _ in enumerate(self.states):
                self.states[row_index_2][column_index] = 2

            # Set the element itself to selected.
            self.states[row_index][column_index] = 1

        # Return a reference to the object, the method was initially called on.
        return self

    def is_crossed_out(self, row: int, column: int) -> bool:
        """
        Method to check if an element at the given row and column is already
        crossed out or not.
        For this the value in the states matrix is used.

        Will return True if crossed out, otherwise returns False.

        Arguments:
            row {int} -- The row index.
            column {int} -- The column index.

        Returns:
            bool -- Wether or not the element is crossed out.
        """

        # Consult the states matrix for the value of the given elements,
        # location.
        state = self.states[row][column]

        # If state == 2 (crossed out) -> return True, else return False
        # return state not in [0, 1]
        return state == 2

        # if state in [0, 1]:
        #     return False
        # else:
        #     return True


# -------------------------------- Functions ----------------------------------


# ----------------------------------- Main ------------------------------------
if __name__ == "__main__":
    pass
