def maximum_deviation(code):
    dev_list = []
    total = 0
    for char in code:
        if char.upper() == 'U':
            total += 1
            dev_list.append(total)
        elif char.upper() == 'D':
            total -= 1
            dev_list.append(total)
        else:
            dev_list.append(total)

    return min(dev_list), max(dev_list)


def parsons(location):
    infile = open(location, 'r')
    contents = infile.read()
    infile.close()

    lines = contents.split('\n')

    matrix = []

    for line in lines[:-1]:
        matrix.append([char for char in line])

    matrix.reverse()

    result = '*'
    try:
        for column_index in range(1, len(matrix[0]), 2):
            for row_index in range(len(matrix)):
                char = matrix[row_index][column_index]
                if char in ['-', '/', '\\']:
                    if char == '-':
                        result += 'R'
                    elif char == '/':
                        result += 'U'
                    else:
                        result += 'D'
                    break
                else:
                    pass
    except IndexError:
        pass

    return result


def contour(code):
    code = code.upper()

    lower, upper = maximum_deviation(code)
    n_lines = upper - lower

    matrix = [[] for i in range(n_lines)]

    
