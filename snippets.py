def update_dict(dictionary: dict, key, value) -> dict:
    """
    Function to update a set in a dictionary.
    Will check if the key is already in the dictionary.
    If not, just make a new set from the value and update the dictionary
    If the key is already in the dictionary, get the value_set add the new
    value and put the key:value pair back.

    Arguments:
        dictionary {dict} -- [description]
        key {[type]} -- [description]
        value {[type]} -- [description]

    Returns:
        dict -- [description]
    """
    if key in dictionary.keys():
        value_set = dictionary[key]
        value_set.add(value)
        value = value_set

    else:
        value = {value, }

    return dictionary.update({key: value})


# Reading from a csv:
# Works like a normal text file

# Open, read and close the file
infile = open('us_population.csv', 'r')
contents = infile.read()
infile.close()

# Split on the new line
lines = contents.split('\n')
lines2 = []
# Split each line again on the separator (',')
for line in lines:
    lines2.append(line.split(','))
