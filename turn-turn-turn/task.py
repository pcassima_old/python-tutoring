bitstring = input()
rotation = int(input())

new = bitstring

if rotation >= 0:
    # Rotate clockwise
    while rotation > 0:
        end = new[-1]
        new = end + new[:-1]
        rotation -= 1
else:
    # rotate counter-clockwise
    while rotation < 0:
        start = new[0]
        new = new[1:] + start
        rotation += 1

line_string = ''
for i in range(len(bitstring)):
    if bitstring[i] != new[i]:
        line_string += '|'
    else:
        line_string += ' '

n = line_string.count('|')
line_string += ' (' + str(n) + ')'

print(bitstring)
print(line_string)
print(new)
