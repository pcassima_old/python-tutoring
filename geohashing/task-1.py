"""
Write a function geo2dec that takes a geohash (str) and returns its decimal
value (int).



Copyright (C) 2019, Pieter-Jan Cassiman

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# --------------------------------- Imports -----------------------------------


# ----------------------------- Global variables ------------------------------

__author__ = "P. Cassiman"
__version__ = '0.0.0'

str2int = {
    "0": 0,
    "1": 1,
    "2": 2,
    "3": 3,
    "4": 4,
    "5": 5,
    "6": 6,
    "7": 7,
    "8": 8,
    "9": 9,
    "b": 10,
    "c": 11,
    "d": 12,
    "e": 13,
    "f": 14,
    "g": 15,
    "h": 16,
    "j": 17,
    "k": 18,
    "m": 19,
    "n": 20,
    "p": 21,
    "q": 22,
    "r": 23,
    "s": 24,
    "t": 25,
    "u": 26,
    "v": 27,
    "w": 28,
    "x": 29,
    "y": 30,
    "z": 31,
}

example = "ezs42"


# --------------------------------- Classes -----------------------------------


# -------------------------------- Functions ----------------------------------


def get_last_char(string: str) -> str:
    """
    Functions returns the last character from a string.
    This is achieved by using the "-1" array index (wraps around to the end).

    Arguments:
        string {str} -- String from which the last character has to be found.

    Returns:
        str -- The last character from the string.
    """
    # Isolate the last character.
    last_char = string[-1]
    # Return the last character.
    return last_char


def remove_last_char(string: str) -> str:
    """
    Function to remove the last character from a string. Returns the whole
    string without the last character.

    Two methods are demonstrated here;
    1. Go over the original string and copy the characters to a new (result)
       string, but stopping before the last character is copied.
    2. Using array slicing.

    Arguments:
        string {str} -- The string from which the last character has to be
                        removed.

    Returns:
        str -- A copy of the original string, with the last character
                 removed.
    """
    # Create a new empty string to store the result.
    result = ""
    # Store the length of the string, minus 1 to deal with array indexing;
    # length starts at "1" but indexes start at "0".
    length = len(string) - 1
    # For-loop going over the original string.
    for i in range(length):
        # As long as we have not reach the end of the original string, copy a
        # character to the new string.
        if i < length:
            # Copy the character at index "i" to the new string.
            result += string[i]
        else:
            # If we are at the last character, stop the loop.
            break

    # Alternative method that uses array slices.
    # result = string[:-1]

    # return the resulting string.
    return result


def decode_char(character: str) -> int:
    """
    Function to decode a character and return its decimal value.
    A dictionary is used to store the value of each character.

    Arguments:
        character {str} -- The character to be decoded.

    Returns:
        int -- The integer value of the character.
    """
    # Get the value that is associated with the character from the dictionary.
    value = str2int.get(character)
    # Return this value.
    return value


def geo2dec(geohash: str) -> int:
    """
    Function to convert geohash to its decimal value.

    Arguments:
        geohash {str} -- The geohash to be converted to a string.

    Returns:
        int -- The decimal value of geohash string.
    """

    # Initialize the variable for the result.
    result = 0
    # Start an index for the power. The result is val * (32 ^ power).
    power = 0

    # As long as there are characters in the geohash, keep looping.
    while (geohash):
        # Get the last character of the geohash.
        character = get_last_char(geohash)
        # Decode the value of the character.
        value = decode_char(character)
        # Add the value, multiplied with the correct power of 32, to the
        # result.
        result += (value * (32 ** power))
        # Remove the last character from the geohash.
        geohash = remove_last_char(geohash)
        # Increment the power variable.
        power += 1

    # After all characters have been decoded and added to the result,
    # return it.
    return result


# ----------------------------------- Main ------------------------------------


if __name__ == "__main__":
    print(example)
    dec_val = geo2dec(example)
    print(dec_val)
