# Geohash
## Preface
**Geohash** is a *geocoding* system that encodes a geographic location into a
short string of letters and digits. For example, geohash ezs42 encodes for an
area around the coordinate with decimal longitude and latitude (-5.6, 42.6).
Geohashes offer properties like arbitrary precision and the possibility of
gradually removing characters from the end of the code to reduce its size (and
gradually lose precision). As a consequence of the gradual precision
degradation, nearby places will often (but not always) present similar
prefixes. The longer a shared prefix is, the closer the two places are.

Using the area around coordinate (-5.6, 42.6) as an example — indicated by a
red marker on the world map below — here is how it is encoded as a geohash.
First we divide the world map with a vertical line into two halves and
represent each half with a binary value: 0 (left half) and 1 (right half).
Because the location is in the left half, the first bit of the geohash is a
zero (0).

![world map](https://dodona.ugent.be/exercises/1289920756/media/geohash_0.en.thumbnail.png "world map")

To improve the resolution of the location, we subdivide the remaining area
again in two halves. But now we use a horizontal line for the division and also
indicate each half with a binary value: 0 (bottom half) and 1 (top half).
Because the location is in the top half, the second bit of the geohash is a
one (1).

![half world map](https://dodona.ugent.be/exercises/1289920756/media/geohash_1.thumbnail.png "half world map")

We then subdivide the remaining area in two halves, but again use a vertical line. Because the location is in the right half, the third bit of the geohash is also a one (1).

![quarter world map](https://dodona.ugent.be/exercises/1289920756/media/geohash_2.thumbnail.png "quarter world map")

By iteratively subdividing the remaining area in two halves — alternating between vertical and horizontal subdivisions — we double the resolution of the geohash in each step, but also make the geohash one bit longer.

## Assignment

