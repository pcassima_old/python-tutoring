"""
Write a function geo2coord that takes a geohash (str) and returns its
corresponding coordinate (longitude, latitude) as a tuple containing float.

even places = longitude
odd places = latitude

longitude > [-180, 180]
latitude > [-90, 90]

This version isn't written as long as the other examples, to show what a
program can look like.


Copyright (C) 2019, Pieter-Jan Cassiman

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# --------------------------------- Imports -----------------------------------


# ----------------------------- Global variables ------------------------------

__author__ = "P. Cassiman"
__version__ = '0.0.0'

example = "ezs42"

str2int = {
    "0": 0,  "1": 1,  "2": 2,  "3": 3,
    "4": 4,  "5": 5,  "6": 6,  "7": 7,
    "8": 8,  "9": 9,  "b": 10, "c": 11,
    "d": 12, "e": 13, "f": 14, "g": 15,
    "h": 16, "j": 17, "k": 18, "m": 19,
    "n": 20, "p": 21, "q": 22, "r": 23,
    "s": 24, "t": 25, "u": 26, "v": 27,
    "w": 28, "x": 29, "y": 30, "z": 31,
}


# --------------------------------- Classes -----------------------------------


# -------------------------------- Functions ----------------------------------


def geo2dec(geohash: str) -> int:
    """
    Function to convert geohash to its decimal value.

    Arguments:
        geohash {str} -- The geohash to be converted to a string.

    Returns:
        int -- The decimal value of geohash string.
    """

    # Initialize the variable for the result.
    result = 0
    # Start an index for the power. The result is val * (32 ^ power).
    power = 0

    # As long as there are characters in the geohash, keep looping.
    while (geohash):

        # Get the last character of the geohash.
        character = geohash[-1]

        # Decode the value of the character.
        value = str2int.get(character.lower())

        # Add the value, multiplied with the correct power of 32, to the
        # result.
        result += (value * (32 ** power))

        # Remove the last character from the geohash.
        geohash = geohash[:-1]

        # Increment the power variable.
        power += 1

    # After all characters have been decoded and added to the result,
    # return it.
    return result


def geo2bin(geohash: str) -> str:
    """
    Function to convert a geohash to a binary representation, using the geo2dec
    function. Leading zero's are added to get to the correct length, these
    are remove by the bin() function.

    Arguments:
        geohash {str} -- The geohash to be converted.

    Returns:
        str -- The binary representation of the string.
    """

    # Convert the geohash to the decimal format and then to the binary format.
    result_bin = bin(geo2dec(geohash))[2:]

    required_length = len(geohash) * 5

    missing_length = required_length - len(result_bin)

    result_bin = ("0" * missing_length) + result_bin

    # Return the binary string.
    return result_bin


def unravel(string: str) -> tuple:
    """
    Function to split a given string into two strings.
    The first string will contain all the characters at the even positions,
    while the second one will contain all the characters at the odd positions.

    Arguments:
        string {str} -- The given string to be split into even and odd
                        characters.

    Returns:
        tuple -- A tuple containing the two strings (even, odd)
    """

    # Initializing the variables.
    even_chars = ""
    odd_chars = ""

    # A for loop to go over the source string.
    for i in range(len(string)):

        # At the odd locations add the characters to the odd string.
        # (the module will return one, which equals to True in comparisons)
        if (i % 2):
            odd_chars += string[i]

        # In all other cases add the character to the even string.
        else:
            even_chars += string[i]

    # Return the result as a tuple.
    return (even_chars, odd_chars)


def bin2coord(bitstring: str, l: float, u: float) -> tuple:
    """
    This function halves the interval, with limits given by l and u, according
    to the given bitstring. The rules to half the interval are determined
    above.

    Arguments:
        bitstring {str} -- The bitstring containing the instructions for
                           halving the interval.
        l {float} -- The starting lower limit of the interval.
        u {float} -- The starting upper limit of the interval.

    Returns:
        tuple -- A tuple containing two floats, the resulting lower and upper
                 limit.
    """

    # Starting with a for loop, to go over the bit string.
    for i in range(len(bitstring)):

        # Extracting the instruction from the bitstring.
        # instruction = int(bitstring[i])
        instruction = bitstring[i]

        # Calculate the midpoint.
        midpoint = (u + l) / 2

        # Check what the instruction is and take the appropriate action.
        # if instruction:
        if instruction == "1":
            # If the instruction is one (take the top half) the midpoint
            # becomes the new lower limit.
            l = midpoint
        else:
            # If the instruction is one (take the bottom half) the midpoint
            # becomes the new upper limit.
            u = midpoint

    # Return the result as a tuple.
    return (l, u)


def geo2coord(geohash: str) -> tuple:
    """
    Function to retrieve the coordinates from a geohash (longitude and
    latitude). The steps needed to achieve this are:

    1. Get the binary representation of the geohash (bitstring).
    2. Split the bitstring into even and odd characters.
    3. Calculate the longitude by halving the interval ([-180, 180]).
    4. Perform one final midpoint calculation, to get the longitude.
    5. Repeat steps 3 and 4 but for latitude ([-90, 90]).
    6. Return the result.

    Arguments:
        geohash {str} -- The string encoding the location.

    Returns:
        tuple -- The resulting location in degrees (longitude, latitude)
    """

    # Get the longitude and latitude bits from the bitstring
    longitude_bits, latitude_bits = unravel(geo2bin(geohash))

    # Calculate the longitude interval by using the relevant bitstring the
    # half the interval.
    longitude_interval = bin2coord(longitude_bits, -180, 180)
    # Perform one last midpoint calculation for the result
    longitude = (longitude_interval[1] + longitude_interval[0]) / 2

    # Calculate the longitude interval by using the relevant bitstring the
    # half the interval.
    latitude_interval = bin2coord(latitude_bits, -90, 90)
    # Perform one last midpoint calculation for the result
    latitude = (latitude_interval[1] + latitude_interval[0]) / 2

    # Return the longitude and latitude as a tuple.
    return (longitude, latitude)


# ----------------------------------- Main ------------------------------------


if __name__ == "__main__":
    print(example)
    result = geo2coord(example)
    print(result)
