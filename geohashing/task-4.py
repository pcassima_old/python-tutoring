"""
Write a function bin2coord that takes three arguments:
    1. a bitstring (str)
    2. the lower limit "l" (float) of an interval
    3. the upper limit "u" (float) of the interval
The function must return a tuple (tuple) containing "l'" and "u'" (floats) that
describes the interval [l', u'] obtained by repeatedly halving the given
interval [l, u] according to the instructions in the given bitstring.

In order to half the interval according to the bitstring, these rules apply:
A zero in the bitstring means take the bottom half, while a one means taking
the top half.
In order to do this the midpoints needs to be determined. When the lower half
needs to be taken, the original lower limit is kept and the midpoint becomes
the new upper limit.
When the top half needs to be taken, the upper limit remains and the midpoint
becomes the new lower limit.



Copyright (C) 2019, Pieter-Jan Cassiman

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# --------------------------------- Imports -----------------------------------


# ----------------------------- Global variables ------------------------------

__author__ = "P. Cassiman"
__version__ = '0.0.0'

example_bitstring = "0111110000000"
example_l = -180
example_u = 180


# --------------------------------- Classes -----------------------------------


# -------------------------------- Functions ----------------------------------


def calc_midpoint(l: float, u: float) -> float:
    """
    This function calculates the midpoint between the upper and lower limit of
    an interval.

    Arguments:
        l {float} -- The lower limit of the interval.
        u {float} -- The upper limit of the interval.

    Returns:
        float -- The midpoint of the interval.
    """

    # Calculate the midpoint of the interval
    midpoint = (u + l) / 2
    # Return the result
    return midpoint


def bin2coord(bitstring: str, l: float, u: float) -> tuple:
    """
    This function halves the interval, with limits given by l and u, according
    to the given bitstring. The rules to half the interval are determined
    above.

    Arguments:
        bitstring {str} -- The bitstring containing the instructions for
                           halving the interval.
        l {float} -- The starting lower limit of the interval.
        u {float} -- The starting upper limit of the interval.

    Returns:
        tuple -- A tuple containing two floats, the resulting lower and upper
                 limit.
    """

    # Starting with a for loop, to go over the bit string.
    for i in range(len(bitstring)):

        # Extracting the instruction from the bitstring.
        # instruction = int(bitstring[i])
        instruction = bitstring[i]

        # Calculate the midpoint.
        midpoint = calc_midpoint(l, u)

        # Check what the instruction is and take the appropriate action.
        # if instruction:
        if instruction == "1":
            # If the instruction is one (take the top half) the midpoint
            # becomes the new lower limit.
            l = midpoint
        else:
            # If the instruction is one (take the bottom half) the midpoint
            # becomes the new upper limit.
            u = midpoint

    # Store the resulting limits in a tuple.
    result = (l, u)
    # Return the tuple.
    return result


# ----------------------------------- Main ------------------------------------


if __name__ == "__main__":
    print(example_bitstring)
    print(example_l)
    print(example_u)
    result = bin2coord(example_bitstring, example_l, example_u)
    print(result)
