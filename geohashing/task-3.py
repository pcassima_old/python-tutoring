"""
Write a function "unravel" that takes a string (str) argument. If we number the
characters in the given string from left to right, starting at zero, the
function must return a tuple (tuple) containing two strings (str), where the
first string is composed of the characters at the even positions and the second
string from the characters at the odd positions.



Copyright (C) 2019, Pieter-Jan Cassiman

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# --------------------------------- Imports -----------------------------------


# ----------------------------- Global variables ------------------------------

__author__ = "P. Cassiman"
__version__ = '0.0.0'

example = "ezs42"


# --------------------------------- Classes -----------------------------------


# -------------------------------- Functions ----------------------------------


def unravel(string: str) -> tuple:
    """
    Function to split a given string into two strings.
    The first string will contain all the characters at the even positions,
    while the second one will contain all the characters at the odd positions.

    Arguments:
        string {str} -- The given string to be split into even and odd
                        characters.

    Returns:
        tuple -- A tuple containing the two strings (even, odd)
    """

    # Initializing the variables.
    even_chars = ""
    odd_chars = ""

    # A for loop to go over the source string.
    for i in range(len(string)):

        # At the odd locations add the characters to the odd string.
        # (the module will return one, which equals to True in comparisons)
        if (i % 2):
            odd_chars += string[i]

        # In all other cases add the character to the even string.
        else:
            even_chars += string[i]

    # Creating a tuple for the result.
    result = (even_chars, odd_chars)
    # Return the result.
    return result


# ----------------------------------- Main ------------------------------------


if __name__ == "__main__":
    print(example)
    result = unravel(example)
    print(result)
