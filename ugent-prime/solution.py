"""
For the 200th anniversary of the Ghent University a special prime number was
designed. The prime number only contains the digits 1 and 8 (9000 in total, as
a reference to the postal code of Ghent, Belgium). However there is also a
single occurrence of the digit 2 in the prime number.

The input for the program is a text file depicting an image made up of two
characters. The two characters that make up the image are mentioned on the
first line


Copyright (C) 2019, Pieter-Jan Cassiman

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# --------------------------------- Imports -----------------------------------


# ----------------------------- Global variables ------------------------------

__author__ = "P. Cassiman"
__version__ = '1.0.0'

# if __name__ == "__main__":
# Reading the input from the terminal.
image = input("Enter your first two figures: ")
# Split off the first accepted character.
number1 = image[0]
# Split off the second accepted character.
number2 = image[1]

# Start the row counter.
row = 1
# Start the column counter.
column = 1
# Start the variable for the foreign character.
newcharacter = "nothing"

while newcharacter == "nothing":
    # As long as no foreign character has been found keep looping.
    # Read a line in the image.
    image = input("enter image: ")

    # Remove all instances of the first accepted character.
    # Also create a copy of the image line (to keep the original).
    line_mod = image.replace(number1, "")
    # Remove all instances of the second accepted character.
    line_mod = line_mod.replace(number2, "")

    if line_mod:
        # If something is left in the line after removing the accepted
        # characters, we know there is a foreign character in there.
        for letter in image:
            # Go over each character in the original image line.
            if not((letter == number1) or (letter == number2)):
                # If the letter is neither of the accepter characters, we
                # have found the foreign character.
                newcharacter = letter
                # Stop the loop.
                break
            else:
                # If the character is either one of the accepted ones,
                # increment the column counter and continue.
                column += 1
    else:
        # If the line is empty after removing the accepted characters,
        # there is no foreign character. So we increment the row counter
        # and continue the loop.
        row += 1
# after we have found the foreign character, we print the result to the
# screen.
print("character '{}' only occurs at row {} and column {}"
        .format(newcharacter, row, column))
