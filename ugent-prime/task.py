"""
For the 200th anniversary of the Ghent University a special prime number was
designed. The prime number only contains the digits 1 and 8 (9000 in total, as
a reference to the postal code of Ghent, Belgium). However there is also a
single occurrence of the digit 2 in the prime number.

The input for the program is a text file depicting an image made up of two
characters. The two characters that make up the image are mentioned on the
first line


Copyright (C) 2019, Pieter-Jan Cassiman

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# --------------------------------- Imports -----------------------------------


# ----------------------------- Global variables ------------------------------

__author__ = "P. Cassiman"
__version__ = '1.0.0'

# --------------------------------- Classes -----------------------------------


# -------------------------------- Functions ----------------------------------


def check_line(char_1: str, char_2: str, line: str) -> bool:
    """
    Function to check if a line contains any characters other than the two
    characters defined in the function parametres.

    Arguments:
        char_1 {str} -- The first accepted character
        char_2 {str} -- The second accepted character
        line {str} -- The string to check

    Returns:
        bool -- Wether or not a "foreign" character was found.
    """

    # Remove the first accepted character for the line (all occurrences).
    line_mod = line.replace(char_1, "")
    # Remove the second accepted character for the line (all occurrences).
    line_mod = line_mod.replace(char_2, "")

    if line_mod:
        # If the line still contains anything, at least one foreign character
        # has been found.
        return True
    else:
        # Otherwise no foreign characters have been found.
        return False

# ----------------------------------- Main ------------------------------------


if __name__ == "__main__":
    # Open the text file with the text making up the image.
    image = open("input.txt", "r")
    # Get the text from the file.
    image_text = image.read()
    # Split up the text into lines (split on the 'newline' character).
    lines = image_text.split("\n")
    # The first line are the accepted characters.
    characters = lines[0]
    # Remove the accepted characters from the lines.
    lines.pop(0)
    # Get the first accepted character.
    char_1 = characters[0]
    # Get the second accepted character.
    char_2 = characters[1]

    # Start the row counter.
    row_counter = 1
    # Create a variable for the row containing the foreign character.
    target_line = ""

    # Loop over all the lines in the image.
    for line in lines:
        # Check each line with the function written above.
        if check_line(char_1, char_2, line):
            # If a foreign character is found save the line
            target_line = line
            # And stop the loop.
            break
        # If no foreign character is found, increment the row_counter.
        row_counter += 1

    # Rinse and repeat for the columns.
    # Start the column counter.
    column_counter = 1
    # Create a variable for the foreign character.
    target_column = ""
    # Loop over all the characters in the line.
    for letter in target_line:
        # If the letter is not the first nor the second accepted character.
        if not ((letter == char_1) or (letter == char_2)):
            # save the forgeign letter.
            target_column = letter
            # And stop the loop.
            break
        # If the letter is either one of the foreign, increment the counter.
        column_counter += 1

    # Print the result to the screen.
    print("character '{}' only occurs at row {} and column {}"
          .format(target_column, row_counter, column_counter))
