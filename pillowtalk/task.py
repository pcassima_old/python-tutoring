import string


def ngram_single(word: str, number: int = 4) -> list:

    pieces_list = []

    while word:
        if len(word) >= number:
            piece = word[0:number]
            pieces_list.append(piece)
            word = word[1:]
        else:
            break

    return pieces_list


def ngram_list(list_of_words, number):

    pieces_list = []

    if isinstance(list_of_words, str):
        # It's one single word
        pieces_list = ngram_single(list_of_words, number)

    elif isinstance(list_of_words, list):
        # Go over each element in the list
        for word in list_of_words:
            pieces_list += ngram_single(word, number)

    return pieces_list


def ngrams(data, number):
    for character in data:
        if character not in string.ascii_letters:
            data = data.replace(character, "-")

    word_list = data.split("-")

    result = ngram_list(word_list, number)

    return result


def dictionary(textfile, number):
    # Open the file
    word_file = open(textfile, 'r')
    # Read the file
    words = word_file.read()
    # Close the file
    word_file.close()

    # split the file
    word_list = words.split("\n")

    result = {}

    for word in word_list:
        # for each word:
        word = word.lower()
        # Make the word lowercase
        word_ngrams = ngrams(word, number)
        # Make the ngrams
        for word_ngram in word_ngrams:
            # For each ngram
            word_ngram = word_ngram.upper()
            # Capitalize the ngram.
            if word_ngram in result.keys():
                # Check if it is already in the dict (keys)
                # If it is in the dict, update the value.
                ngram_value = result[word_ngram]
                ngram_value.add(word)
                result[word_ngram] = ngram_value
            else:
                # If it is not, create a new key-value pair
                ngram_value = set()
                ngram_value.add(word)
                result[word_ngram] = ngram_value

    # return the dictionary
    return result
