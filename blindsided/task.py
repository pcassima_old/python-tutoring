"""
An empty python file that can be used as a template.

Copyright (C) 2019, [copyright holder (author)]

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# --------------------------------- Imports -----------------------------------

from __future__ import annotations
from typing import Union

# ----------------------------- Global variables ------------------------------

__author__ = "N. Surname"
__version__ = '0.0.0'

# --------------------------------- Classes -----------------------------------


class Pile(object):
    # Create a list with all valid value-strings for the cards
    values = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K']
    # Create a list with all strings for the suits
    suits = ['C', 'D', 'H', 'S']

    def __init__(self,
                 description: Union[list, tuple, set],
                 flipped: list = None):
        """
        Initializer for the pile object.
        The flipped argument is positional and is used for saving the flipped
        state when creating new pile objects from already existing ones.

        Arguments:
            description {Union[list, tuple, set]} -- The cards in the pile.

        Keyword Arguments:
            flipped {list} -- The flipped state for the cards
                              (default: {None}).
        """
        # Create a list to store all the cards associated with the object.
        self.cards = []
        # If flipped is not defined.
        if not flipped:
            # Create a empty flipped list
            self.flipped = []
            # For each card in the description.
            for card in description:
                # Add the card to the cards list.
                self.cards.append(card.upper())
                # Check if the suit is lower or upper case.
                suit = card[-1]
                if suit.islower():
                    # If it is lower case add, true to the flipped list.
                    self.flipped.append(True)
                else:
                    # If it is upper case, add false to the flipped list.
                    self.flipped.append(False)
        else:
            # If flipped is defined, use it as the flipped list.
            self.flipped = flipped
            for card in description:
                # Add the card to the cards list.
                self.cards.append(card)

        # If everything is right, we can go over each card in the
        # description.

    def __str__(self) -> str:
        """
        Method to return a string representation of the stack of cards.
        For this, each card is added to the string, separated by a space.

        Returns:
            str -- All the cards in the stack represented as a string.
        """

        # Start with a blank string
        result = ''

        # Go over the cards list and keep track of the index.
        for i in range(len(self.cards)):

            # If the card is flipped, represent it as '**'.
            if self.flipped[i]:
                card = '**'

            # If the card is not flipped, just show the card.
            else:
                card = self.cards[i]

            # Add each card to the string.
            result += card
            # Followed by a space.
            result += ' '
        # Finally remove the trailing space.
        result = result[:-1]
        # Return the result.
        return result

    def __repr__(self) -> str:
        """
        Returns the classname as well as the representation of the cards in the
        deck, as a string.

        Returns:
            str -- The verbose representation of the string.
        """

        # Start with a string and place the class name in it.
        result = '{}'.format(self.__class__.__name__)

        # create a copy of the cards list (for safety reason).
        list_copy = self.cards.copy()

        # Go over the cards list and keep track of the index.
        for i in range(len(self.cards)):
            # If the card is flipped, make it with lower case.
            if self.flipped[i]:
                list_copy[i] = list_copy[i].lower()
            # If the card is not flipped, show it with upper case.
            else:
                list_copy[i] = list_copy[i].upper()

        # Add a string representation of the list to the string.
        result += '({})'.format(str(list_copy))

        # Return the string.
        return result

    def __eq__(self, other: Pile) -> bool:
        """
        Method to check if two piles are equal. Two piles are equal if they
        have the same amount of face up cards.

        Arguments:
            other {Pile} -- The Pile object to compare to.

        Returns:
            bool -- True if both piles have the same amount of face up cards.
        """

        return self.faceUp() == other.faceUp()

    def faceUp(self) -> int:
        """
        Method that returns the amount of cards that are face up.

        Returns:
            int -- The amount of face up cards in the pile.
        """

        # Initialize the counter.
        counter = 0

        # Go over the flipped list (with booleans).
        for flip in self.flipped:
            # If the value is False, the cards is not flipped and thus face up.
            if not flip:
                # Increment the counter.
                counter += 1

        # Return the counter.
        return counter

    def split(self, n: int = None) -> tuple:
        """
        Method to split pile into two stacks.
        - If no position is given, the position is equal to the amount of
          face-up cards.
        - If an integer is given, split the stack at the given position.

        Keyword Arguments:
            n {int} -- The position, on which to split the pile
                       (default: {None}).

        Returns:
            tuple -- A tuple containing two new Pile objects, containing the
                     new piles after splitting.
        """

        # If the position is not an integer, split at the face-up amount of
        # cards - position.
        if not isinstance(n, int):
            n = self.faceUp()

        # Use array slicing to split the pile.
        # Take the first n-amount of cards for the first sub-pile.
        stack1 = self.cards[:n]
        stack1_flipped = self.flipped[:n]

        # Take the other cards for the second sub-pile.
        stack2 = self.cards[n:]
        stack2_flipped = self.flipped[n:]

        # Create a new Pile object for each newly created stack.
        stack1_object = self.__class__(stack1, stack1_flipped)
        stack2_object = self.__class__(stack2, stack2_flipped)

        # Return both objects as a tuple.
        return stack1_object, stack2_object

    def flip(self, position: Union[int, list, tuple, set] = None) -> self:
        """
        Method to flip cards in the pile.
        - if no position is given, all the cards have to be flipped.
        - if an integer is given, the card at that position has to be flipped.
        - if an collection is given, the cards at the positions in the
          collection have to be flipped.

        Keyword Arguments:
            position {Union[int, list, tuple, set]} --
                The position of which cards have to be flipped.

        Returns:
            self -- A reference to the object on which the method was called.
        """

        # If an integer is given.
        if isinstance(position, int):
            # Flip the card at the given position.
            self.flipped[position] = not self.flipped[position]

        # If a collection is given.
        elif isinstance(position, (tuple, list, set)):
            # Flip each card at the positions in the given collection.
            for number in position:
                self.flipped[number] = not self.flipped[number]

        # If no position is given.
        elif not position:
            # Flip all cards.
            for i in range(len(self.flipped)):
                self.flipped[i] = not self.flipped[i]

        # Return a reference to the object on which the method was called.
        return self


# -------------------------------- Functions ----------------------------------


# ----------------------------------- Main ------------------------------------

if __name__ == "__main__":
    pass
