# Python tutoring
These are exercises solved in tutoring sessions. All exercises originate from Dodona (University of Ghent). Besides the exercises there also some [cheatsheets](https://gitlab.com/pcassima/python-tutoring/tree/master/cheatsheets "python cheatsheets") that can be found in their own folder. These contain information about several aspects of Python.

